# NHL94Hockey.com Open Source

This site is set up in an MVC style, leveraging Services to interact with the Database.

## Prereqs
* Knowledge of PHP 5.3+ (namespaces)
* Knowledge of MySQL 5
* Understanding of MVC design patterns
* Understanding of [Twig Templates](http://twig.sensiolabs.org/documentation)
* Understanding of [Flight Routes](http://flightphp.com/learn) or Routing in general (sinatra/express/flask)

## Code Flow
1. All URLs are redirected (by apache) to index.php with the URL itself (/game/23) as a parameter
2. index.php loads app/bootstrap.php, which initializes Twig, Flight, Sendgrid (if you need it), and the Database
3. All routes are loaded into index.php
4. Flight matches the route (url) to a Controller function
5. Controller asks a Service for data, then displays it by calling a View
6. View is twig/html which is compiled and displayed to the user as plain HTML

## Architecture
* Services interact with the database. All saving, reading, updating goes on through a service
* Models are PHP Object representations of a database table
* Controllers handle user input, fetching what they want to see
* Routes match a URL to a Controller's handling function (for example: /game/34 would call GameController's getGameDetails function with 34 as the id parameter)
* Extractors parse hex files for a given system
* Views are just HTML