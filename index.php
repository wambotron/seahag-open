<?php
define('ROOT', realpath(dirname(__FILE__)));

require_once 'app/bootstrap.php';
require_once 'app/routes/UserRoutes.php';
require_once 'app/routes/TeamRoutes.php';
require_once 'app/routes/GameRoutes.php';
require_once 'app/routes/SnesRoutes.php';
require_once 'app/routes/GensRoutes.php';
require_once 'app/routes/ApiRoutes.php';

use \app\controllers\UserController;
use \app\services\RankingService;


/* Homepage */
Flight::route('/', function() {
    echo Flight::get('twig')->render('index.twig');
});

/* FAQs */
Flight::route('/faqs/', function() {
    echo Flight::get('twig')->render('faqs.twig');
});

/* About */
Flight::route('/about/', function() {
    echo Flight::get('twig')->render('about.twig');
});

Flight::map('notFound', function() {
    echo Flight::get('twig')->render('404.twig');
});

Flight::before('start', function() {
    if (isset($_COOKIE['userName']) && !empty($_COOKIE['userName'])) {
        $userController = new UserController(Flight::get('twig'));
        $userController->setupNotifications();
    }
});

Flight::start();
 ?>
