<?php
namespace app\services;
use \app\models\NHL94GameStat;

use \mysqli;
use \PDO;
use \PDOException;

class StatService {
    /* make this easy on the queries */
    const NHL_STAT_TABLE = 'nhl94_game_stats';
    const NHL_STAT_ID = 'id';
    const NHL_STAT_GAME = 'game';
    const NHL_STAT_HOME_USER = 'home_user';
    const NHL_STAT_VISITOR_USER = 'visitor_user';
    const NHL_STAT_HOME_TEAM = 'home_team';
    const NHL_STAT_VISITOR_TEAM = 'visitor_team';
    const NHL_STAT_HOME_SCORE = 'home_score';
    const NHL_STAT_VISITOR_SCORE = 'visitor_score';
    const NHL_STAT_HOME_P1_GOALS = 'home_p1_goals';
    const NHL_STAT_HOME_P2_GOALS = 'home_p2_goals';
    const NHL_STAT_HOME_P3_GOALS = 'home_p3_goals';
    const NHL_STAT_HOME_OT_GOALS = 'home_ot_goals';
    const NHL_STAT_VISITOR_P1_GOALS = 'visitor_p1_goals';
    const NHL_STAT_VISITOR_P2_GOALS = 'visitor_p2_goals';
    const NHL_STAT_VISITOR_P3_GOALS = 'visitor_p3_goals';
    const NHL_STAT_VISITOR_OT_GOALS = 'visitor_ot_goals';
    const NHL_STAT_HOME_PP_GOALS = 'home_pp_goals';
    const NHL_STAT_VISITOR_PP_GOALS = 'visitor_pp_goals';
    const NHL_STAT_HOME_PP = 'home_pp';
    const NHL_STAT_VISITOR_PP = 'visitor_pp';
    const NHL_STAT_HOME_ATTACK_ZONE = 'home_attack_zone';
    const NHL_STAT_VISITOR_ATTACK_ZONE = 'visitor_attack_zone';
    const NHL_STAT_HOME_PP_TIME = 'home_pp_time';
    const NHL_STAT_VISITOR_PP_TIME = 'visitor_pp_time';
    const NHL_STAT_HOME_PP_SHOTS = 'home_pp_shots';
    const NHL_STAT_VISITOR_PP_SHOTS = 'visitor_pp_shots';
    const NHL_STAT_HOME_SH_GOALS = 'home_sh_goals';
    const NHL_STAT_VISITOR_SH_GOALS = 'visitor_sh_goals';
    const NHL_STAT_HOME_FACEOFFS_WON = 'home_faceoffs_won';
    const NHL_STAT_VISITOR_FACEOFFS_WON = 'visitor_faceoffs_won';
    const NHL_STAT_HOME_BODY_CHECKS = 'home_body_checks';
    const NHL_STAT_VISITOR_BODY_CHECKS = 'visitor_body_checks';
    const NHL_STAT_HOME_PASSES_RECEIVED = 'home_passes_received';
    const NHL_STAT_VISITOR_PASSES_RECEIVED = 'visitor_passes_received';
    const NHL_STAT_HOME_PASSES = 'home_passes';
    const NHL_STAT_VISITOR_PASSES = 'visitor_passes';
    const NHL_STAT_HOME_BREAKAWAYS = 'home_breakaways';
    const NHL_STAT_VISITOR_BREAKAWAYS = 'visitor_breakaways';
    const NHL_STAT_HOME_BREAKAWAY_GOALS = 'home_breakaway_goals';
    const NHL_STAT_VISITOR_BREAKAWAY_GOALS = 'visitor_breakaway_goals';
    const NHL_STAT_HOME_PENALTY_SHOTS = 'home_penalty_shots';
    const NHL_STAT_VISITOR_PENALTY_SHOTS = 'visitor_penalty_shots';
    const NHL_STAT_HOME_PENALTY_SHOT_GOALS = 'home_penalty_shot_goals';
    const NHL_STAT_VISITOR_PENALTY_SHOT_GOALS = 'visitor_penalty_shot_goals';
    const NHL_STAT_HOME_ONE_TIMERS = 'home_one_timers';
    const NHL_STAT_VISITOR_ONE_TIMERS = 'visitor_one_timers';
    const NHL_STAT_HOME_ONE_TIMER_GOALS = 'home_one_timer_goals';
    const NHL_STAT_VISITOR_ONE_TIMER_GOALS = 'visitor_one_timer_goals';
    const NHL_STAT_HOME_SHOTS = 'home_shots';
    const NHL_STAT_VISITOR_SHOTS = 'visitor_shots';
    const NHL_STAT_PENALTY_SUMMARY = 'penalty_summary';
    const NHL_STAT_SCORING_SUMMARY = 'scoring_summary';
    
    protected $db = null;
    public function __construct($db = null) {
        if (!isset($db)) {
            throw new Exception("Database Connection Error.");
        }
        
        $this->db = $db;
    }
    
    /**
     * Save the GameStats to the db
     * 
     * @param NHL94GameStat $stats
     */
    public function saveNHL94Game($stats) {
        $result = false;
        
        if (!empty($stats)) {
            $sql = "INSERT INTO ". self::NHL_STAT_TABLE ." (".
                    self::NHL_STAT_GAME .",".
                    self::NHL_STAT_HOME_USER .",".
                    self::NHL_STAT_VISITOR_USER .",".
                    self::NHL_STAT_HOME_TEAM .",".
                    self::NHL_STAT_VISITOR_TEAM .",".
                    self::NHL_STAT_HOME_SCORE .",".
                    self::NHL_STAT_VISITOR_SCORE .",".
                    self::NHL_STAT_HOME_P1_GOALS .",".
                    self::NHL_STAT_HOME_P2_GOALS .",".
                    self::NHL_STAT_HOME_P3_GOALS .",".
                    self::NHL_STAT_HOME_OT_GOALS .",".
                    self::NHL_STAT_VISITOR_P1_GOALS .",".
                    self::NHL_STAT_VISITOR_P2_GOALS .",".
                    self::NHL_STAT_VISITOR_P3_GOALS .",".
                    self::NHL_STAT_VISITOR_OT_GOALS .",".
                    self::NHL_STAT_HOME_PP_GOALS .",".
                    self::NHL_STAT_VISITOR_PP_GOALS .",".
                    self::NHL_STAT_HOME_PP .",".
                    self::NHL_STAT_VISITOR_PP .",".
                    self::NHL_STAT_HOME_ATTACK_ZONE .",".
                    self::NHL_STAT_VISITOR_ATTACK_ZONE .",".
                    self::NHL_STAT_HOME_PP_TIME .",".
                    self::NHL_STAT_VISITOR_PP_TIME .",".
                    self::NHL_STAT_HOME_PP_SHOTS .",".
                    self::NHL_STAT_VISITOR_PP_SHOTS .",".
                    self::NHL_STAT_HOME_SH_GOALS .",".
                    self::NHL_STAT_VISITOR_SH_GOALS .",".
                    self::NHL_STAT_HOME_FACEOFFS_WON .",".
                    self::NHL_STAT_VISITOR_FACEOFFS_WON .",".
                    self::NHL_STAT_HOME_BODY_CHECKS .",".
                    self::NHL_STAT_VISITOR_BODY_CHECKS .",".
                    self::NHL_STAT_HOME_PASSES_RECEIVED .",".
                    self::NHL_STAT_VISITOR_PASSES_RECEIVED .",".
                    self::NHL_STAT_HOME_PASSES .",".
                    self::NHL_STAT_VISITOR_PASSES .",".
                    self::NHL_STAT_HOME_BREAKAWAYS .",".
                    self::NHL_STAT_VISITOR_BREAKAWAYS .",".
                    self::NHL_STAT_HOME_BREAKAWAY_GOALS .",".
                    self::NHL_STAT_VISITOR_BREAKAWAY_GOALS .",".
                    self::NHL_STAT_HOME_PENALTY_SHOTS .",".
                    self::NHL_STAT_VISITOR_PENALTY_SHOTS .",".
                    self::NHL_STAT_HOME_PENALTY_SHOT_GOALS .",".
                    self::NHL_STAT_VISITOR_PENALTY_SHOT_GOALS .",".
                    self::NHL_STAT_HOME_ONE_TIMERS .",".
                    self::NHL_STAT_VISITOR_ONE_TIMERS .",".
                    self::NHL_STAT_HOME_ONE_TIMER_GOALS .",".
                    self::NHL_STAT_VISITOR_ONE_TIMER_GOALS .",".
                    self::NHL_STAT_HOME_SHOTS .",".
                    self::NHL_STAT_VISITOR_SHOTS .",".
                    self::NHL_STAT_PENALTY_SUMMARY .",".
                    self::NHL_STAT_SCORING_SUMMARY
                    .") VALUES (".
                    $stats->game .",".
                    $stats->homeUser .",".
                    $stats->visitorUser .",".
                    $stats->homeTeam .",".
                    $stats->visitorTeam .",".
                    $stats->homeScore .",".
                    $stats->visitorScore .",".
                    $stats->homeGoalsP1 .",".
                    $stats->homeGoalsP2 .",".
                    $stats->homeGoalsP3 .",".
                    $stats->homeGoalsOT .",".
                    $stats->visitorGoalsP1 .",".
                    $stats->visitorGoalsP2 .",".
                    $stats->visitorGoalsP3 .",".
                    $stats->visitorGoalsOT .",".
                    $stats->homePPGoals .",".
                    $stats->visitorPPGoals .",".
                    $stats->homePP .",".
                    $stats->visitorPP .",".
                    $stats->homeAttackZone .",".
                    $stats->visitorAttackZone .",".
                    $stats->homePPTime .",".
                    $stats->visitorPPTime .",".
                    $stats->homePPShots .",".
                    $stats->visitorPPShots .",".
                    $stats->homeSHGoals .",".
                    $stats->visitorSHGoals .",".
                    $stats->homeFaceoffsWon .",".
                    $stats->visitorFaceoffsWon .",".
                    $stats->homeBodyChecks .",".
                    $stats->visitorBodyChecks .",".
                    $stats->homePassesReceived .",".
                    $stats->visitorPassesReceived .",".
                    $stats->homePasses .",".
                    $stats->visitorPasses .",".
                    $stats->homeBreakaways .",".
                    $stats->visitorBreakaways .",".
                    $stats->homeBreakawayGoals .",".
                    $stats->visitorBreakawayGoals .",".
                    $stats->homePenaltyShots .",".
                    $stats->visitorPenaltyShots .",".
                    $stats->homePenaltyShotGoals .",".
                    $stats->visitorPenaltyShotGoals .",".
                    $stats->homeOneTimers .",".
                    $stats->visitorOneTimers .",".
                    $stats->homeOneTimerGoals .",".
                    $stats->visitorOneTimerGoals .",".
                    $stats->homeShots .",".
                    $stats->visitorShots .",".
                    "'". $stats->penaltySummary ."',".
                    "'". $stats->scoringSummary ."')";
    
            try {
                # Start the transaction
                $this->db->beginTransaction();
                $this->db->exec($sql);
                $this->db->commit();

                $result = true;
            } catch (PDOException $e) {
                $this->db->rollback();
                #echo $e->getMessage();
            }
        }
        
        return $result;
    }
    
    public function deleteNHLGameStatById($id) {
        $result = false;
        
        # Check to see if they gave us a real ID
        if (!empty($id) && is_numeric($id)) {
            try {
                # remove from games table
                $s = $this->db->prepare("DELETE FROM `". self::NHL_STAT_TABLE ."` WHERE `". self::NHL_STAT_GAME ."` = :id");
                $s->bindParam(':id', $id, PDO::PARAM_INT);
                $s->execute();
                $result = true;
            } catch (PDOException $e) {
                #echo $e->getMessage();
            }
        }
        
        return $result;
    }
}