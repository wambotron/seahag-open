<?php

namespace app\services;

use \app\services\TeamService;
use \app\services\UserService;
use \app\services\StatService;
use \app\models\Game;

use \PDO;
use \PDOException;


class GameService {
    
    /* make this easy on the queries */
    const GAME_TABLE = "games";
    const GAME_ID = "id";
    const GAME_HOME_USER = "home_user";
    const GAME_VISITOR_USER = "visitor_user";
    const GAME_HOME_SCORE = "home_score";
    const GAME_VISITOR_SCORE = "visitor_score";
    const GAME_HOME_TEAM = "home_team";
    const GAME_VISITOR_TEAM = "visitor_team";
    const GAME_IS_OT = "ot";
    const GAME_CONFIRMED = "confirmed";
    const GAME_UPLOADER = "uploader";
    const GAME_SYSTEM = "system";
    const RECENT_GAMES = 10;
    
    protected $db = null;

    public function __construct($db = null) {
        if (!isset($db)) {
            throw new Exception("Database Connection Error.");
        }
        
        $this->db = $db;
    }
    
    /**
     * Create a new game
     * 
     * @param Game $game
     * @return Integer
     */
    public function createNewGame($game) {
        $result = 0;
        
        /* If they sent in a game */
        if (isset($game)) {
            $sql =  "INSERT INTO ". self::GAME_TABLE ." (".
                    self::GAME_HOME_USER .",".
                    self::GAME_VISITOR_USER .",".
                    self::GAME_HOME_SCORE .",".
                    self::GAME_VISITOR_SCORE .",".
                    self::GAME_HOME_TEAM .",".
                    self::GAME_VISITOR_TEAM .",".
                    self::GAME_IS_OT .",".
                    self::GAME_CONFIRMED .",".
                    self::GAME_UPLOADER .",".
                    self::GAME_SYSTEM
                    .") VALUES (".
                    $game->homeUser .",".
                    $game->visitorUser .",".
                    $game->homeScore .",".
                    $game->visitorScore .",".
                    $game->homeTeam .",".
                    $game->visitorTeam .",".
                    $game->isOT .",".
                    $game->confirmed .",".
                    $game->uploader .",".
                    $game->system .")";

            try {
                # Start the transaction
                $this->db->beginTransaction();
                $this->db->exec($sql);
                $result = $this->db->lastInsertId();
                $this->db->commit();
            } catch (PDOException $e) {
                $this->db->rollback();
                $result = 0;
                #echo $e->getMessage();
            }
        }
        return $result;
    }
    
    /**
     * Reset a game"s confirmation
     * 
     * @param Integer $id
     */
    public function resetGameConfirmation($id) {
        return $this->setConfirmation($id, 0);
    }
    
    /**
     * Confirm a game"s score
     * 
     * @param Integer $id
     */
    public function confirmGame($id) {
        return $this->setConfirmation($id, 1);
    }
    
    /**
     * Deny a game"s score
     * 
     * @param Integer $id
     */
    public function denyGame($id) {
        return $this->setConfirmation($id, 2);
    }

    private function setConfirmation($id, $confirmation) {
        $result = false;
        
        if (isset($id) && is_numeric($id)) {
            try {
                $s = $this->db->prepare("UPDATE ". self::GAME_TABLE ." SET ". self::GAME_CONFIRMED ."=". $confirmation ."
                    WHERE ". self::GAME_ID ."= :id");
                $s->bindParam(':id', $id, PDO::PARAM_INT);

                $s->execute();
                $result = true;
            } catch (PDOException $e) {
                #echo $e->getMessage();
            }

            $result = true;
        }
        
        return $result;
    }
    
    /**
     * Grab a game by id
     * 
     * @param Integer $id
     * @return Array
     */
    public function getGameById($id) {
        $game = null;
        if (isset($id) && is_numeric($id)) {
            try {
                $s = $this->db->prepare("SELECT * FROM `". self::GAME_TABLE ."` WHERE `". self::GAME_ID ."` = :id");
                $s->bindParam(':id', $id, PDO::PARAM_INT);
                $s->execute();
                $results = $s->fetch(PDO::FETCH_ASSOC);
                if (!empty($results)) {
                    $game = $this->createGameFromRecord($results);
                }
            } catch (PDOException $e) {
                #echo $e->getMessage();
            }

        }
        return $game;
    }
    
    /**
     * Returns all unconfirmed (non-denied/accepted) games for a user
     * 
     * @param Integer $id
     * @return array
     */
    public function getUnconfirmedGamesByUserId($id) {
        $games = array();
        
        if (isset($id) && is_numeric($id)) {
            try {
                $s = $this->db->prepare("SELECT * FROM `". self::GAME_TABLE ."`
                        WHERE (`". self::GAME_HOME_USER ."` = :id
                            OR `". self::GAME_VISITOR_USER ."` = :id)
                        AND `". self::GAME_CONFIRMED ."` = 0
                        AND `". self::GAME_UPLOADER ."` != :id");
                $s->bindParam(':id', $id, PDO::PARAM_INT);
                $s->execute();
                $results = $s->fetchAll(PDO::FETCH_ASSOC);

                if (!empty($results)) {
                    foreach ($results as $row) {
                        $games[] = $this->createGameFromRecord($row);
                    }
                }
            } catch (PDOException $e) {
                #echo $e->getMessage();
            }
        }
        
        return $games;
    }
    
    /**
     * Returns all denied games for a user
     * 
     * @param Integer $id
     * @return array
     */
    public function getDeniedGamesByUserId($id) {
        $games = array();
        
        if (isset($id) && is_numeric($id)) {
            try {
                $s = $this->db->prepare("SELECT * FROM `". self::GAME_TABLE ."`
                        WHERE (`". self::GAME_HOME_USER ."` = :id
                            OR `". self::GAME_VISITOR_USER ."` = :id)
                        AND `". self::GAME_CONFIRMED ."` = 2
                        AND `". self::GAME_UPLOADER ."` = :id");
                $s->bindParam(':id', $id, PDO::PARAM_INT);
                $s->execute();
                $results = $s->fetchAll(PDO::FETCH_ASSOC);

                if (!empty($results)) {
                    foreach ($results as $row) {
                        $games[] = $this->createGameFromRecord($row);
                    }
                }
            } catch (PDOException $e) {
                #echo $e->getMessage();
            }
        }
        
        return $games;
    }
    
    /**
     * Remove a Game from the Games table
     * 
     * @param Integer $id
     * @return Boolean
     */
    public function deleteGameById($id) {
        $result = false;

        # Check to see if they gave us a real ID
        if (!empty($id) && is_numeric($id)) {
            try {
                # remove from games table
                $s = $this->db->prepare("DELETE FROM `". self::GAME_TABLE ."` WHERE `". self::GAME_ID ."` = :id");
                $s->bindParam(':id', $id, PDO::PARAM_INT);
                $s->execute();
                $result = true;
            } catch (PDOException $e) {
                #echo $e->getMessage();
            }
        }
        
        return $result;
    }
    
    private function createGameFromRecord($rec) {
        if (!empty($rec)) {
            return new Game(
                    $rec[self::GAME_ID], 
                    $rec[self::GAME_HOME_USER],
                    $rec[self::GAME_VISITOR_USER],
                    $rec[self::GAME_HOME_SCORE],
                    $rec[self::GAME_VISITOR_SCORE],
                    $rec[self::GAME_HOME_TEAM],
                    $rec[self::GAME_VISITOR_TEAM],
                    $rec[self::GAME_IS_OT],
                    $rec[self::GAME_CONFIRMED],
                    $rec[self::GAME_UPLOADER],
                    $rec[self::GAME_SYSTEM]
                );
        } else {
            return null;
        }
    }
}