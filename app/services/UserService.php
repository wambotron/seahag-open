<?php

namespace app\services;

use \app\services\GameService;
use \app\services\TeamService;

use \app\models\User;

use \PDO;
use \PDOException;

class UserService {
    
    /* make this easy on the queries */
    const USER_TABLE = 'users';
    const USER_ID = 'id';
    const USER_NAME = 'name';
    const USER_EMAIL = 'email';
    const USER_CONTACT = 'contact';
    const USER_PASSWORD = 'password';
    const USER_ROLE = 'role';
    const USER_VERIFIED = 'verified';
    const USER_RATING = 'rating';
    const PASS_RANDOM_LENGTH = 8;
    const PASS_ALLOWED_CHARS = 'abdefghjkmnpqrstuvwxyzABEFGHIJKLMNPRSTUVWXYZ123456789 $%!._-';
    
    const GAME_TABLE = 'games';
    const GAME_SYSTEM = 'system';
    const GAME_HOME_USER = 'home_user';
    const GAME_VISITOR_USER = 'visitor_user';
    
    protected $db = null;

    public function __construct($db = null) {
        if (!isset($db)) {
            throw new Exception("Database Connection Error.");
        }
        
        $this->db = $db;
    }
    
    /**
     * Create a new user in the db
     * 
     * @param User $user
     * @return Integer
     */
    public function createNewUser($user) {
        $result = 0;
        
        /* If they sent in a user */
        if (isset($user)) {
            /* Make sure this user doesn't already exist */
            $existingUser = $this->getUserByName($user->name);
            if (!isset( $existingUser ) && $this->isValidName($user->name)) {
                $result = $this->saveUser($user);
            }
        }
        return $result;
    }
    
    /**
     * Edit a user in the db
     * 
     * @param User $user
     * @return Boolean
     */
    public function editUser($user) {
        $result = false;
        
        /* If they sent in a user */
        if (isset($user)) {
            $this->updateUser($user);
            $result = true;
        }
        return $result;
    }
    
    /**
     * Create a random password for this joker. Abides by some CONST vals.
     * Basically selects from the allowed chars until it hits the required length.
     * 
     * @return String
     */
    public function returnRandomPassword() {
        $chars = self::PASS_ALLOWED_CHARS;
        $charsLength = strlen($chars);
        
        $pass = '';
        for ($i = 0; $i < self::PASS_RANDOM_LENGTH; $i++) {
             $pass .= $chars[rand(0, $charsLength - 1)];
        }
        
        return $pass;
    }
    
    /**
     * Grab a user by their name
     * 
     * @param String $name
     * @return User
     */
    public function getUserByName($name) {
        $user = null;
        if (isset($name)) {
            try {
                $name = trim($name);
                $s = $this->db->prepare("SELECT * FROM `". self::USER_TABLE ."` WHERE `". self::USER_NAME ."` = :name");
                $s->bindParam(':name', $name);
                $s->execute();
                $results = $s->fetch(PDO::FETCH_ASSOC);
                if (!empty($results)) {
                    $user = new User(
                                $results[self::USER_ID], 
                                $results[self::USER_NAME],
                                $results[self::USER_EMAIL],
                                $results[self::USER_CONTACT],
                                $results[self::USER_PASSWORD],
                                $results[self::USER_ROLE],
                                $results[self::USER_RATING]
                            );
                }
            } catch (PDOException $e) {
                #echo $e->getMessage();
            }
        }
        
        return $user;
    }
    
    /**
     * Grab a user by their id
     * 
     * @param Integer $id
     * @return User
     */
    public function getUserById($id) {
        $user = null;
        if (isset($id) && is_numeric($id)) {
            try {
                $s = $this->db->prepare("SELECT * FROM `". self::USER_TABLE ."` WHERE `". self::USER_ID ."` = :id");
                $s->bindParam(':id', $id, PDO::PARAM_INT);
                $s->execute();
                $results = $s->fetch(PDO::FETCH_ASSOC);
                if (!empty($results)) {
                    $user = new User(
                                $results[self::USER_ID], 
                                $results[self::USER_NAME],
                                $results[self::USER_EMAIL],
                                $results[self::USER_CONTACT],
                                $results[self::USER_PASSWORD],
                                $results[self::USER_ROLE],
                                $results[self::USER_RATING]
                            );
                }
            } catch (PDOException $e) {
                #echo $e->getMessage();
            }
        }
        return $user;
    }
    
    /**
     * Check if a user exists in the database with the provided credentials
     * 
     * @param String $name
     * @param String $password
     * @return User
     */
    public function checkUserCredentials($name, $password) {
        $user = null;
        
        /* Check to see they passed in testable credentials */
        if (isset($name) && isset($password)) {
            /* Grab the user by the given name */
            $checkUser = $this->getUserByName($name);
            /* if this user exists, check against the password */
            if (!empty($checkUser) && $checkUser->password == sha1($password)) {
                /* If it's all good, set that user for return */
                $user = $checkUser;
            }
        }
        
        return $user;
    }

    /**
     * Update a user's rating
     *
     * @param Integer $userId
     * @param Integer $rating
     */
    public function updateUserRatingByUserId($userId, $rating) {
        if (is_numeric($userId) && is_numeric($rating)) {
            try {
                $sql = "UPDATE ". self::USER_TABLE ." SET ". self::USER_RATING ."=". trim($rating) ."
                        WHERE ". self::USER_ID ."=". $userId;
                $this->db->exec($sql);
            } catch (PDOException $e) {
                #echo $e->getMessage();
            }
        }
    }
    
    
    /**
     * Saves user to the db
     * 
     * @param User $user
     * @return Integer
     */
    private function saveUser($user) {
        $id = 0;

        $sql = "INSERT INTO ". self::USER_TABLE ."(".
              self::USER_NAME .",".
              self::USER_EMAIL .",".
              self::USER_CONTACT .",".
              self::USER_PASSWORD .",".
              self::USER_ROLE .",".
              self::USER_RATING ."
              ) VALUES (
              '". trim($user->name) ."',
              '". trim($user->email) ."',
              '". trim($user->contact) ."',
              '". sha1(trim($user->password)) ."',
              '". trim($user->role) ."',
              '". trim($user->rating) ."')";

        try {
            # Start the transaction
            $this->db->beginTransaction();
            $this->db->exec($sql);
            $id = $this->db->lastInsertId();
            $this->db->commit();
        } catch (PDOException $e) {
            $this->db->rollback();
            #echo $e->getMessage();
            $id = 0;
        }
        return $id;
    }
    
    /**
     * Updates user to the db
     * 
     * @param User $user
     */
    private function updateUser($user) {
        $sql = "UPDATE ". self::USER_TABLE ." SET ".
                self::USER_NAME ."=". trim($user->name) .",".
                self::USER_EMAIL ."=". trim($user->email) .",".
                self::USER_CONTACT ."=". trim($user->contact) .",".
                self::USER_PASSWORD ."=". sha1(trim($user->password)) .",".
                self::USER_ROLE ."=". trim($user->role) .",".
                self::USER_RATING ."=". trim($user->rating) ."
                WHERE ". self::USER_ID ."=". $user->id;


        try {
            # Start the transaction
            $this->db->beginTransaction();
            $this->db->exec($sql);
            $this->db->commit();

            $result = true;
        } catch (PDOException $e) {
            $this->db->rollback();
            #echo $e->getMessage();
        }
    }
    
    /**
     * Let's just keep it alphanumerical
     * 
     * @param String $name
     */
    private function isValidName($name) {
        return preg_match("/^\w+$/", $name);
    }
}