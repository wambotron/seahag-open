<?php

namespace app\services;

use \PDO;
use \PDOException;

class JsonDataService {
    
    const DEFAULT_LIMIT = 10;
    const FULL_GAME_DETAILS = "h.id as home_user_id, h.name as home_user_name, 
                        v.id as visitor_user_id, v.name as visitor_user_name, 
                        g.id as game_id, g.ot as isOT, g.confirmed, g.uploader,
                        sys.name as system_name,
                        ht.id as home_team_id,
                        ht.city as home_team_city,
                        ht.name as home_team_name,
                        vt.id as visitor_team_id,
                        vt.city as visitor_team_city,
                        vt.name as visitor_team_name,
                        s.home_score, s.visitor_score,
                        s.home_p1_goals, s.home_p2_goals, s.home_p3_goals, s.home_ot_goals, s.home_pp_goals, s.home_pp, s.home_pp_time, s.home_pp_shots, s.home_sh_goals, s.home_faceoffs_won, s.home_body_checks, s.home_attack_zone, s.home_passes, s.home_passes_received, s.home_breakaways, s.home_breakaway_goals, s.home_penalty_shots, s.home_penalty_shot_goals, s.home_one_timers, s.home_one_timer_goals, s.home_shots,
                        s.visitor_p1_goals, s.visitor_p2_goals, s.visitor_p3_goals, s.visitor_ot_goals, s.visitor_pp_goals, s.visitor_pp, s.visitor_pp_time, s.visitor_pp_shots, s.visitor_sh_goals, s.visitor_faceoffs_won, s.visitor_body_checks, s.visitor_attack_zone, s.visitor_passes, s.visitor_passes_received, s.visitor_breakaways, s.visitor_breakaway_goals, s.visitor_penalty_shots, s.visitor_penalty_shot_goals, s.visitor_one_timers, s.visitor_one_timer_goals, s.visitor_shots,
                        s.penalty_summary, s.scoring_summary";
    const BASIC_GAME_DETAILS = "h.id as home_user_id, h.name as home_user_name, 
                        v.id as visitor_user_id, v.name as visitor_user_name, 
                        g.id as game_id, g.home_score, g.visitor_score, g.ot as isOT, g.confirmed, g.uploader,
                        sys.name as system_name,
                        ht.city as home_team_city,
                        ht.name as home_team_name,
                        vt.city as visitor_team_city,
                        vt.name as visitor_team_name";
    
    protected $db = null;

    public function __construct($db = null) {
        if (!isset($db)) {
            throw new Exception("Database Connection Error.");
        }
        
        $this->db = $db;
    }

    /*
     * Get confirmed games from $start to $start + $limit
     * 
     * @param Integer $start
     * @param Integer $limit
     * 
     * return String
     */
    public function getConfirmedGamesFull($start = 0, $limit = self::DEFAULT_LIMIT) {
        $json = "[]";
        $sql = "SELECT ". self::FULL_GAME_DETAILS ."
                    FROM users as h, games as g, users as v, nhl94_game_stats as s, systems as sys, teams as ht, teams as vt
                    WHERE g.confirmed = 1
                       AND h.id = g.home_user
                       AND (v.id != h.id AND v.id = g.visitor_user)
                       AND g.id = s.game
                       AND sys.id = g.system
                       AND ht.id = g.home_team
                       AND vt.id = g.visitor_team
                ORDER BY g.id DESC
                LIMIT $start, $limit";
        try {
            $s = $this->db->query($sql);
            $results = $s->fetchAll(PDO::FETCH_ASSOC);
            
            if ($results && count($results) > 0) {
                $json = json_encode($results);
            }

        } catch (PDOException $e) {
            #echo $e->getMessage();
        }
        
        
        return "{ \"games\": ". $json ." }";
        
    }

    /*
     * Get confirmed games from $start to $start + $limit
     * 
     * @param Integer $start
     * @param Integer $limit
     * 
     * return String
     */
    public function getConfirmedGamesBasic($start = 0, $limit = self::DEFAULT_LIMIT) {
        $json = "[]";
        $sql = "SELECT ". self::BASIC_GAME_DETAILS ."
                    FROM users as h, games as g, users as v, systems as sys, teams as ht, teams as vt
                    WHERE g.confirmed = 1
                       AND h.id = g.home_user
                       AND (v.id != h.id AND v.id = g.visitor_user)
                       AND sys.id = g.system
                       AND ht.id = g.home_team
                       AND vt.id = g.visitor_team
                ORDER BY g.id DESC
                LIMIT $start, $limit";
        try {
            $s = $this->db->query($sql);
            $results = $s->fetchAll(PDO::FETCH_ASSOC);
            
            if ($results && count($results) > 0) {
                $json = json_encode($results);
            }

        } catch (PDOException $e) {
            #echo $e->getMessage();
        }
        
        return "{ \"games\": ". $json ." }";
        
    }
    
    /*
     * Get game by game id
     * 
     * @param Integer $gameId
     * 
     * return String
     */
    public function getAllGameDetailsByGameId($gameId = 0) {
        $json = "[]";
        
        if ($gameId && is_numeric($gameId)) {
            try {
                $s = $this->db->prepare("SELECT ". self::FULL_GAME_DETAILS ."
                        FROM users as h, games as g, users as v, nhl94_game_stats as s, systems as sys, teams as ht, teams as vt
                        WHERE g.id = :game_id
                           AND h.id = g.home_user
                           AND (v.id != h.id AND v.id = g.visitor_user)
                           AND g.id = s.game
                           AND sys.id = g.system
                           AND ht.id = g.home_team
                           AND vt.id = g.visitor_team
                    ORDER BY g.id DESC");
                $s->bindParam(":game_id", $gameId);
                $s->execute();
                $results = $s->fetchAll(PDO::FETCH_ASSOC);
                if ($results && count($results) > 0) {
                    $json = json_encode($results);
                }
            } catch (PDOException $e) {
                #echo $e->getMessage();
            }
        }
        
        return "{ \"games\": ". $json ." }";
        
    }
    
    /*
     * Get game by game id
     * 
     * @param Integer $gameId
     * 
     * return String
     */
    public function getBasicGameDetailsByGameId($gameId = 0) {
        $json = "[]";
        
        if ($gameId && is_numeric($gameId)) {
            try {
                $s = $this->db->prepare("SELECT ". self::BASIC_GAME_DETAILS ."
                            FROM users as h, games as g, users as v, systems as sys, teams as ht, teams as vt
                            WHERE g.id = :game_id
                               AND h.id = g.home_user
                               AND (v.id != h.id AND v.id = g.visitor_user)
                               AND sys.id = g.system
                               AND ht.id = g.home_team
                               AND vt.id = g.visitor_team
                        ORDER BY g.id DESC");
                $s->bindParam(":game_id", $gameId);
                $s->execute();
                $results = $s->fetchAll(PDO::FETCH_ASSOC);
                if ($results && count($results) > 0) {
                    $json = json_encode($results);
                }
            } catch (PDOException $e) {
                #echo $e->getMessage();
            }
        }
        
        return "{ \"games\": ". $json ." }";
        
    }

    /*
     * Get games by user from $start to $start + $limit
     * 
     * @param Integer $userId
     * @param Integer $start
     * @param Integer $limit
     * 
     * return String
     */
    public function getGamesByUserId($userId = 0, $start = 0, $limit = self::DEFAULT_LIMIT) {
        $json = "[]";
        
        if ($userId && is_numeric($userId)) {
            try {
                $s = $this->db->prepare("SELECT ". self::FULL_GAME_DETAILS ."
                            FROM users as h, games as g, users as v, nhl94_game_stats as s, systems as sys, teams as ht, teams as vt
                            WHERE (h.id = :user_id OR v.id = :user_id) 
                               AND h.id = g.home_user
                               AND (v.id != h.id AND v.id = g.visitor_user)
                               AND g.id = s.game
                               AND sys.id = g.system
                               AND ht.id = g.home_team
                               AND vt.id = g.visitor_team
                        ORDER BY g.id DESC
                        LIMIT $start, $limit");
                $s->bindParam(":user_id", $userId);
                $s->execute();
                $results = $s->fetchAll(PDO::FETCH_ASSOC);

                if ($results && count($results) > 0) {
                    $json = json_encode($results);
                }
            } catch (PDOException $e) {
                #echo $e->getMessage();
            }
        }
        
        return "{ \"games\": ". $json ." }";
        
    }

    /*
     * Get games by user and confirmation status from $start to $start + $limit
     * 
     * @param Integer $userId
     * @param Integer $confirmationStatus
     * @param Integer $start
     * @param Integer $limit
     * 
     * return String
     */
    public function getGamesByUserIdAndConfirmationStatus($userId = 0, $confirmationStatus = 100, $start = 0, $limit = self::DEFAULT_LIMIT) {
        $json = "[]";
        
        if ($userId && is_numeric($userId)) {
            try {
                $s = $this->db->prepare("SELECT ". self::FULL_GAME_DETAILS ."
                            FROM users as h, games as g, users as v, nhl94_game_stats as s, systems as sys, teams as ht, teams as vt
                            WHERE g.confirmed = :conf_status
                               AND (h.id = :user_id or v.id = :user_id) 
                               AND h.id = g.home_user
                               AND (v.id != h.id AND v.id = g.visitor_user)
                               AND g.id = s.game
                               AND sys.id = g.system
                               AND ht.id = g.home_team
                               AND vt.id = g.visitor_team
                        ORDER BY g.id DESC
                        LIMIT $start, $limit");
                $s->bindParam(":conf_status", $confirmationStatus);
                $s->bindParam(":user_id", $userId);
                $s->execute();
                $results = $s->fetchAll(PDO::FETCH_ASSOC);

                if ($results && count($results) > 0) {
                    $json = json_encode($results);
                }
            } catch (PDOException $e) {
                #echo $e->getMessage();
            }
        }
        
        return "{ \"games\": ". $json ." }";
        
    }
    
    /*
     * Get confirmed games by user from $start to $start + $limit
     * 
     * @param Integer $userId
     * @param Integer $start
     * @param Integer $limit
     * 
     * return String
     */
    public function getConfirmedGamesByUserId($userId, $start = 0, $limit = self::DEFAULT_LIMIT) {
        return $this->getGamesByUserIdAndConfirmationStatus($userId, 1, $start, $limit);
    }
    
    /*
     * Get unconfirmed games by user from $start to $start + $limit
     * 
     * @param Integer $userId
     * @param Integer $start
     * @param Integer $limit
     * 
     * return String
     */
    public function getUnconfirmedGamesByUserId($userId, $start = 0, $limit = self::DEFAULT_LIMIT) {
        return $this->getGamesByUserIdAndConfirmationStatus($userId, 0, $start, $limit);
    }

    public function getDeniedGamesByUserId($userId, $start = 0, $limit = self::DEFAULT_LIMIT) {
        return $this->getGamesByUserIdAndConfirmationStatus($userId, 2, $start, $limit);
    }

    public function getUnconfirmedAndDeniedGamesByUserId($userId, $start = 0, $limit = self::DEFAULT_LIMIT) {
        $json = "[]";
        
        if ($userId && is_numeric($userId)) {
            try {
                $s = $this->db->prepare("SELECT ". self::BASIC_GAME_DETAILS ."
                            FROM users as h, games as g, users as v, nhl94_game_stats as s, systems as sys, teams as ht, teams as vt
                            WHERE ((g.confirmed = :denied AND g.uploader = :user_id)
                                    OR (g.confirmed = :unconfirmed AND g.uploader != :user_id))
                               AND (h.id = :user_id or v.id = :user_id) 
                               AND h.id = g.home_user
                               AND (v.id != h.id AND v.id = g.visitor_user)
                               AND g.id = s.game
                               AND sys.id = g.system
                               AND ht.id = g.home_team
                               AND vt.id = g.visitor_team
                        ORDER BY g.id DESC
                        LIMIT $start, $limit");
                $unconfirmedStatus = 0;
                $deniedStatus = 2;
                $s->bindParam(":unconfirmed", $unconfirmedStatus);
                $s->bindParam(":denied", $deniedStatus);
                $s->bindParam(":user_id", $userId);
                $s->execute();
                $results = $s->fetchAll(PDO::FETCH_ASSOC);

                if ($results && count($results) > 0) {
                    $json = json_encode($results);
                }
            } catch (PDOException $e) {
                #echo $e->getMessage();
            }
        }

        return "{ \"games\": ". $json ." }";
    }

    /**
     * Get games by user from $start to $start + $limit
     * 
     * @param Integer $teamId
     * @param Integer $userId
     * @param Integer $start
     * @param Integer $limit
     * 
     * return String
     */
    public function getGamesAsTeamByUserId($teamId = 0, $userId = 0, $start = 0, $limit = self::DEFAULT_LIMIT) {
        $json = "[]";
        
        if ($userId && is_numeric($userId) && $teamId && is_numeric($teamId)) {
            try {
                $s = $this->db->prepare("SELECT ". self::FULL_GAME_DETAILS ."
                            FROM users as h, games as g, users as v, nhl94_game_stats as s, systems as sys, teams as ht, teams as vt
                            WHERE (h.id = :user_id
                                AND h.id = g.home_user
                                AND (v.id != h.id AND v.id = g.visitor_user)
                                AND g.id = s.game
                                AND sys.id = g.system
                                AND ht.id = :team_id
                                AND ht.id = g.home_team
                                AND vt.id = g.visitor_team)
                            OR (v.id = :user_id
                                AND (h.id = g.home_user)
                                AND (v.id != h.id AND v.id = g.visitor_user)
                                AND g.id = s.game
                                AND sys.id = g.system
                                AND ht.id = g.home_team
                                AND vt.id = :team_id
                                AND vt.id = g.visitor_team)
                        ORDER BY g.id DESC
                        LIMIT $start, $limit");
                $s->bindParam(":user_id", $userId);
                $s->bindParam(":team_id", $teamId);
                $s->execute();
                $results = $s->fetchAll(PDO::FETCH_ASSOC);

                if ($results && count($results) > 0) {
                    $json = json_encode($results);
                }
            } catch (PDOException $e) {
                #echo $e->getMessage();
            }
        }
        
        return "{ \"games\": ". $json ." }";
        
    }

    /*
     * Get games by system from $start to $start + $limit
     * 
     * @param Integer $systemId
     * @param Integer $start
     * @param Integer $limit
     * 
     * return String
     */
    public function getGamesBySystemId($systemId = 0, $start = 0, $limit = self::DEFAULT_LIMIT) {
        $json = "[]";
        
        if ($systemId && is_numeric($systemId)) {
            try {

                $s = $this->db->prepare("SELECT ". self::BASIC_GAME_DETAILS ."
                        FROM users as h, games as g, users as v, systems as sys, teams as ht, teams as vt
                        WHERE sys.id = :system_id
                           AND h.id = g.home_user
                           AND (v.id != h.id AND v.id = g.visitor_user)
                           AND sys.id = g.system
                           AND ht.id = g.home_team
                           AND vt.id = g.visitor_team
                        ORDER BY g.id DESC
                        LIMIT $start, $limit");
                $s->bindParam(":system_id", $systemid);
                $s->execute();
                $results = $s->fetchAll(PDO::FETCH_ASSOC);

                if ($results && count($results) > 0) {
                    $json = json_encode($results);
                }
            } catch (PDOException $e) {
                #echo $e->getMessage();
            }
        }
        
        return "{ \"games\": ". $json ." }";
        
    }

    /*
     * Get games by user and confirmation status from $start to $start + $limit
     * 
     * @param Integer $systemId
     * @param Integer $confirmationStatus
     * @param Integer $start
     * @param Integer $limit
     * 
     * return String
     */
    public function getGamesBySystemIdAndConfirmationStatus($systemId = 0, $confirmationStatus = false, $start = 0, $limit = self::DEFAULT_LIMIT) {
        $json = "[]";
        
        if ($systemId && is_numeric($systemId)) {
            try {
                $s = $this->db->prepare("SELECT ". self::BASIC_GAME_DETAILS ."
                            FROM users as h, games as g, users as v, systems as sys, teams as ht, teams as vt
                            WHERE g.confirmed = :conf_status
                               AND sys.id = :system_id
                               AND h.id = g.home_user
                               AND (v.id != h.id AND v.id = g.visitor_user)
                               AND sys.id = g.system
                               AND ht.id = g.home_team
                               AND vt.id = g.visitor_team
                        ORDER BY g.id DESC
                        LIMIT $start, $limit");
                $s->bindParam(":conf_status", $confirmationStatus);
                $s->bindParam(":system_id", $systemId);
                $s->execute();
                $results = $s->fetchAll(PDO::FETCH_ASSOC);

                if ($results && count($results) > 0) {
                    $json = json_encode($results);
                }
            } catch (PDOException $e) {
                #echo $e->getMessage();
            }
        } 
        
        return "{ \"games\": ". $json ." }";
        
    }
    
    /*
     * Get confirmed games by user from $start to $start + $limit
     * 
     * @param Integer $systemId
     * @param Integer $start
     * @param Integer $limit
     * 
     * return String
     */
    public function getConfirmedGamesBySystemId($systemId, $start = 0, $limit = self::DEFAULT_LIMIT) {
        return $this->getGamesBySystemIdAndConfirmationStatus($systemId, 1, $start, $limit);
    }
    
    /*
     * Get unconfirmed games by user from $start to $start + $limit
     * 
     * @param Integer $systemId
     * @param Integer $start
     * @param Integer $limit
     * 
     * return String
     */
    public function getUnconfirmedGamesBySystemId($systemId, $start = 0, $limit = self::DEFAULT_LIMIT) {
        return $this->getGamesBySystemIdAndConfirmationStatus($systemId, 0, $start, $limit);
    }
    
    /*
     * Get games by team from $start to $start + $limit
     * 
     * @param Integer $teamId
     * @param Integer $start
     * @param Integer $limit
     * 
     * return String
     */
    public function getGamesByTeamId($teamId = 0, $start = 0, $limit = self::DEFAULT_LIMIT) {
        $json = "[]";
        
        if ($teamId && is_numeric($teamId)) {
            try {
                $s = $this->db->prepare("SELECT ". self::FULL_GAME_DETAILS ."
                            FROM users as h, games as g, users as v, nhl94_game_stats as s, systems as sys, teams as ht, teams as vt
                            WHERE (ht.id = :team_id OR vt.id = :team_id)
                               AND h.id = g.home_user
                               AND (v.id != h.id AND v.id = g.visitor_user)
                               AND g.id = s.game
                               AND sys.id = g.system
                               AND ht.id = g.home_team
                               AND vt.id = g.visitor_team
                        ORDER BY g.id DESC
                        LIMIT $start, $limit");
                $s->bindParam(":team_id", $teamId);
                $s->execute();
                $results = $s->fetchAll(PDO::FETCH_ASSOC);

                if ($results && count($results) > 0) {
                    $json = json_encode($results);
                }
            } catch (PDOException $e) {
                #echo $e->getMessage();
            }
        }
        
        return "{ \"games\": ". $json ." }";
        
    }
    
    /*
     * Get games by team and confirmation status from $start to $start + $limit
     * 
     * @param Integer $teamId
     * @param Integer $confirmationStatus
     * @param Integer $start
     * @param Integer $limit
     * 
     * return String
     */
    public function getGamesByTeamIdAndConfirmationStatus($teamId = 0, $confirmationStatus = false, $start = 0, $limit = self::DEFAULT_LIMIT) {
        $json = "[]";
        
        if ($teamId && is_numeric($teamId)) {
            try {
                $s = $this->db->prepare("SELECT ". self::FULL_GAME_DETAILS ."
                        FROM users as h, games as g, users as v, nhl94_game_stats as s, systems as sys, teams as ht, teams as vt
                        WHERE g.confirmed = :conf_status
                           AND (ht.id = :team_id OR vt.id = :team_id)
                           AND h.id = g.home_user
                           AND (v.id != h.id AND v.id = g.visitor_user)
                           AND g.id = s.game
                           AND sys.id = g.system
                           AND ht.id = g.home_team
                           AND vt.id = g.visitor_team
                        ORDER BY g.id DESC
                        LIMIT $start, $limit");
                $s->bindParam(":conf_status", $confirmationStatus);
                $s->bindParam(":team_id", $teamId);
                $s->execute();
                $results = $s->fetchAll(PDO::FETCH_ASSOC);

                if ($results && count($results) > 0) {
                    $json = json_encode($results);
                }
            } catch (PDOException $e) {
                #echo $e->getMessage();
            }
        }
        
        return "{ \"games\": ". $json ." }";
        
    }
    
    /*
     * Get confirmed games by team from $start to $start + $limit
     * 
     * @param Integer $teamId
     * @param Integer $start
     * @param Integer $limit
     * 
     * return String
     */
    public function getConfirmedGamesByTeamId($teamId, $start = 0, $limit = self::DEFAULT_LIMIT) {
        return $this->getGamesByTeamIdAndConfirmationStatus($teamId, 1, $start, $limit);
    }
    
    /*
     * Get unconfirmed games by user from $start to $start + $limit
     * 
     * @param Integer $teamId
     * @param Integer $start
     * @param Integer $limit
     * 
     * return String
     */
    public function getUnconfirmedGamesByTeamId($teamId, $start = 0, $limit = self::DEFAULT_LIMIT) {
        return $this->getGamesByTeamIdAndConfirmationStatus($teamId, 0, $start, $limit);
    }
    
    /*
     * Get unverified games by user from $start to $start + $limit
     * 
     * @param Integer $teamId
     * @param Integer $start
     * @param Integer $limit
     * 
     * return String
     */
    public function getUnverifiedGamesByTeamId($teamId, $start = 0, $limit = self::DEFAULT_LIMIT) {
        return $this->getGamesByTeamIdAndConfirmationStatus($teamId, 5, $start, $limit);
    }
    
    public function getBasicUserDetailsByUserId($userId) {
        $json = "[]";
        try {
            $s = $this->db->prepare("SELECT id, name FROM users
                    WHERE id = :user_id");
            $s->bindParam(":user_id", $userId);
            $s->execute();
            $results = $s->fetchAll(PDO::FETCH_ASSOC);

            if ($results && count($results) > 0) {
                $json = json_encode($results);
            }
        } catch (PDOException $e) {
            #echo $e->getMessage();
        }
        return "{ \"users\": ". $json ." }";
    }
    
    public function getFullUserDetailsByUserId($userId) {
        $json = "[]";
        try {
            $s = $this->db->prepare("SELECT id, name, email, contact, rating FROM users
                    WHERE id = :user_id");
            $s->bindParam(":user_id", $userId);
            $s->execute();
            $results = $s->fetchAll(PDO::FETCH_ASSOC);

            if ($results && count($results) > 0) {
                $json = json_encode($results);
            }
        } catch (PDOException $e) {
            #echo $e->getMessage();
        }
        
        return "{ \"users\": ". $json ." }";
    }
    
    public function getBasicUserDetailsByUserName($name) {
        $json = "[]";
        try {
            $s = $this->db->prepare("SELECT id, name FROM users
                    WHERE name = :name");
            $s->bindParam(":name", $name);
            $s->execute();
            $results = $s->fetchAll(PDO::FETCH_ASSOC);

            if ($results && count($results) > 0) {
                $json = json_encode($results);
            }
        } catch (PDOException $e) {
            #echo $e->getMessage();
        }
        return "{ \"users\": ". $json ." }";
    }
    
    public function getFullUserDetailsByUserName($name) {
        $json = "[]";
        try {
            $s = $this->db->prepare("SELECT id, name, email, contact, rating FROM users
                    WHERE name = :name");
            $s->bindParam(":name", $name);
            $s->execute();
            $results = $s->fetchAll(PDO::FETCH_ASSOC);

            if ($results && count($results) > 0) {
                $json = json_encode($results);
            }
        } catch (PDOException $e) {
            #echo $e->getMessage();
        }
        
        return "{ \"users\": ". $json ." }";
    }
    
    
    public function getAllUserMinimalDetails() {
        $json = "[]";
        $sql = "SELECT id, name FROM users ORDER BY name ASC";
        try {
            $s = $this->db->query($sql);
            $results = $s->fetchAll(PDO::FETCH_ASSOC);

            if ($results && count($results) > 0) {
                $json = json_encode($results);
            }
        } catch (PDOException $e) {
            #echo $e->getMessage();
        }
        
        return "{ \"users\": ". $json ." }";
    }
    
    /*
     * Grabs the recent game history (10 games) for everyone
     */
    public function getAllUsersRecentRecords() {
        $json = "[]";

        try {
            
            $sql = "SELECT h.id as home_user_id, h.name as home_user_name, h.rating as home_user_rating,
                        v.id as visitor_user_id, v.name as visitor_user_name, v.rating as visitor_user_rating,
                        g.home_score, g.visitor_score, g.ot as isOT
                    FROM users as h, games as g, users as v
                        WHERE g.confirmed = 1
                           AND h.id = g.home_user
                           AND (v.id != h.id AND v.id = g.visitor_user)
                    ORDER BY g.id DESC";
            $s = $this->db->query($sql);
            $results = $s->fetchAll(PDO::FETCH_ASSOC);
            if ($results && count($results) > 0) {
                
                $userRecords = array();
                
                foreach ($results as $result) {
                    $hu = $result['home_user_id'];
                    $vu = $result['visitor_user_id'];
                    $hun = $result['home_user_name'];
                    $vun = $result['visitor_user_name'];
                    $hur = $result['home_user_rating'];
                    $vur = $result['visitor_user_rating'];
                    
                    /* Make an entry for a user if they don't exist */
                    if (!array_key_exists($hu, $userRecords)) {
                        $userRecords[$hu] = array(
                            'id' => $hu,
                            'name' => $hun,
                            'games' => 0,
                            'wins' => 0,
                            'losses' => 0,
                            'ties' => 0,
                            'otlosses' => 0,
                            'points' => 0,
                            'rating' => $hur
                        );
                    }
                    
                    if (!array_key_exists($vu, $userRecords)) {
                        $userRecords[$vu] = array(
                            'id' => $vu,
                            'name' => $vun,
                            'games' => 0,
                            'wins' => 0,
                            'losses' => 0,
                            'ties' => 0,
                            'otlosses' => 0,
                            'points' => 0,
                            'rating' => $vur
                        );
                    }
                    
                    /* Once we fill ten games, we skip their stats */
                    $fillHome = $userRecords[$hu]['games'] < 10;
                    $fillVisitor = $userRecords[$vu]['games'] < 10;
                    if ($fillHome || $fillVisitor) {
                        $hs = $result['home_score'];
                        $vs = $result['visitor_score'];
                        $ot = $result['isOT'];
                        
                        # Add to the game total
                        $userRecords[$hu]['games']++;
                        $userRecords[$vu]['games']++;
                        
                        # Home won
                        if ($hs > $vs) {
                            
                            if ($fillHome) {
                                $userRecords[$hu]['wins']++;
                                $userRecords[$hu]['points'] += 2;
                            }
                            
                            if ($fillVisitor) {
                                $userRecords[$vu]['losses']++;
                            }
                            
                        # Visitor won
                        } else if ($vs > $hs) {
                            
                            if ($fillVisitor) {
                                $userRecords[$vu]['wins']++;
                                $userRecords[$vu]['points'] += 2;
                            }
                            
                            if ($fillHome) {
                                $userRecords[$hu]['losses']++;
                            }
                            
                        # Tie!
                        } else if ($hs == $vs) {
                            
                            if ($fillHome) {
                                $userRecords[$hu]['ties']++;
                                $userRecords[$hu]['points']++;
                            }
                            
                            if ($fillVisitor) {
                                $userRecords[$vu]['ties']++;
                                $userRecords[$vu]['points']++;
                            }
                            
                        }
                    }
                }
                uksort($userRecords, function($a, $b) use ($userRecords) {
                    return $userRecords[$b]['rating'] - $userRecords[$a]['rating'];
                });
                
                $json = json_encode(array_values($userRecords));
            }
        } catch (PDOException $e) {
            #echo $e->getMessage();
        }
        
        return "{ \"users\": ". $json ." }";
    }
    
    /*
     * Grabs the recent game history (10 games) for everyone by system
     */
    public function getAllUsersRecentRecordsBySystemId($systemId) {
        $json = "[]";
        
        try {
            $s = $this->db->prepare("SELECT h.id as home_user_id, h.name as home_user_name, h.rating as home_user_rating,
                        v.id as visitor_user_id, v.name as visitor_user_name, v.rating as visitor_user_rating,
                        g.home_score, g.visitor_score, g.ot as isOT
                    FROM users as h, games as g, users as v, systems as sys
                        WHERE g.confirmed = 1
                           AND sys.id = :system_id
                           AND h.id = g.home_user
                           AND (v.id != h.id AND v.id = g.visitor_user)
                           AND sys.id = g.system
                    ORDER BY g.id DESC");
            $s->bindParam(":system_id", $systemId);
            $s->execute();
            $results = $s->fetchAll(PDO::FETCH_ASSOC);
            if ($results && count($results) > 0) {
                
                $userRecords = array();
                foreach ($results as $result) {
                    $hu = $result['home_user_id'];
                    $vu = $result['visitor_user_id'];
                    $hun = $result['home_user_name'];
                    $vun = $result['visitor_user_name'];
                    $hur = $result['home_user_rating'];
                    $vur = $result['visitor_user_rating'];
                    
                    /* Make an entry for a user if they don't exist */
                    if (!array_key_exists($hu, $userRecords)) {
                        $userRecords[$hu] = array(
                            'id' => $hu,
                            'name' => $hun,
                            'games' => 0,
                            'wins' => 0,
                            'losses' => 0,
                            'ties' => 0,
                            'otlosses' => 0,
                            'points' => 0,
                            'rating' => $hur
                        );
                    }
                    
                    if (!array_key_exists($vu, $userRecords)) {
                        $userRecords[$vu] = array(
                            'id' => $vu,
                            'name' => $vun,
                            'games' => 0,
                            'wins' => 0,
                            'losses' => 0,
                            'ties' => 0,
                            'otlosses' => 0,
                            'points' => 0,
                            'rating' => $vur
                        );
                    }
                    
                    /* Once we fill ten games, we skip their stats */
                    $fillHome = $userRecords[$hu]['games'] < 10;
                    $fillVisitor = $userRecords[$vu]['games'] < 10;
                    if ($fillHome || $fillVisitor) {
                        $hs = $result['home_score'];
                        $vs = $result['visitor_score'];
                        $ot = $result['isOT'];
                        
                        # Add to the game total
                        $userRecords[$hu]['games']++;
                        $userRecords[$vu]['games']++;
                        
                        # Home won
                        if ($hs > $vs) {
                            
                            if ($fillHome) {
                                $userRecords[$hu]['wins']++;
                                $userRecords[$hu]['points'] += 2;
                            }
                            
                            if ($fillVisitor) {
                                $userRecords[$vu]['losses']++;
                            }
                            
                        # Visitor won
                        } else if ($vs > $hs) {
                            
                            if ($fillVisitor) {
                                $userRecords[$vu]['wins']++;
                                $userRecords[$vu]['points'] += 2;
                            }
                            
                            if ($fillHome) {
                                $userRecords[$hu]['losses']++;
                            }
                            
                        # Tie!
                        } else if ($hs == $vs) {
                            
                            if ($fillHome) {
                                $userRecords[$hu]['ties']++;
                                $userRecords[$hu]['points']++;
                            }
                            
                            if ($fillVisitor) {
                                $userRecords[$vu]['ties']++;
                                $userRecords[$vu]['points']++;
                            }
                            
                        }
                    }
                }
                uksort($userRecords, function($a, $b) use ($userRecords) {
                    return $userRecords[$b]['rating'] - $userRecords[$a]['rating'];
                });
                
                $json = json_encode(array_values($userRecords));
            }
        } catch (PDOException $e) {
            #echo $e->getMessage();
        }
        
        return "{ \"users\": ". $json ." }";
    }
    
    public function getTeamsFull() {
        $json = "[]";
        $sql = "SELECT * FROM teams ORDER BY id ASC";
        try {
            $s = $this->db->query($sql);
            $results = $s->fetchAll(PDO::FETCH_ASSOC);
            
            if (!empty($results)) {
                $json = json_encode($results);
            }
        } catch (PDOException $e) {
            #echo $e->getMessage();
        }
        
        return $json;
    }
    
}
