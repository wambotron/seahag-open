<?php

namespace app\services;

use \app\models\UserStat;

class UserStatService {
    
    protected $user;
    protected $games;
    
    public function __construct($user, $games) {
        if (!empty($user) && count($games) > 0) {
            $this->user = $user;
            $this->games = $games;
        }
    }
    
    /**
     * 
     * @param type $user
     * @param type $games
     */
    public function getAllUserStatDetails() {
        
        $teamsPlayed = $this->getTeamsPlayed();
        $teamKeys = array_keys($teamsPlayed);
        if (count($teamKeys) > 0) {
            $favorite = $teamsPlayed[$teamKeys[0]];
        } else {
            $favorite = '';
        }
        
        $userStat = new UserStat();
        $userStat->teamsPlayed = $teamsPlayed;
        $userStat->favorite = $favorite;
        $userStat->rival = $this->getRivalUser();
        $userStat->averages = $this->getAverages();
        $userStat->totals = $this->getTotals();
        
        return $userStat;
    }
    
    /*
     * Grabs the most played/favorite team of a user
     * 
     * @param User $user
     * @return Team
     */
    private function getTeamsPlayed() {
        $teamsPlayed = array();
        
        if (!empty($this->user) && count($this->games) > 0) {
            foreach ($this->games as $game) {
                if ($game->confirmed == 1) {
                    $ht = $game->home_team_id;
                    $vt = $game->visitor_team_id;
                    $hu = $game->home_user_id;
                    $vu = $game->visitor_user_id;
                    $htc = $game->home_team_city;
                    $htn = $game->home_team_name;
                    $vtc = $game->visitor_team_city;
                    $vtn = $game->visitor_team_name;
                    $hs = $game->home_score;
                    $vs = $game->visitor_score;

                    # Setup the teams played array per team
                    if (!array_key_exists($ht, $teamsPlayed)) {
                        $teamsPlayed[$ht] = array(
                            'id' => $ht,
                            'city' => $htc,
                            'name' => $htn,
                            'count' => 0,
                            'wins' => 0,
                            'losses' => 0,
                            'ties' => 0
                        );
                    }
                    if (!array_key_exists($vt, $teamsPlayed)) {
                        $teamsPlayed[$vt] = array(
                            'id' => $vt,
                            'city' => $vtc,
                            'name' => $vtn,
                            'count' => 0,
                            'wins' => 0,
                            'losses' => 0,
                            'ties' => 0
                        );
                    }

                    # Add to the team play count if it was the user's team
                    if ($hu == $this->user->id) {
                        $teamsPlayed[$ht]['count']++;
                        if ($hs > $vs) $teamsPlayed[$ht]['wins']++;
                        else if ($hs < $vs) $teamsPlayed[$ht]['losses']++;
                        else $teamsPlayed[$ht]['ties']++;
                    } else if ($vu == $this->user->id) {
                        $teamsPlayed[$vt]['count']++;
                        if ($vs > $hs) $teamsPlayed[$vt]['wins']++;
                        else if ($vs < $hs) $teamsPlayed[$vt]['losses']++;
                        else $teamsPlayed[$vt]['ties']++;
                    }
                }
            }
            
            # Reverse Sort these baddies by how often they're played
            if (!empty($teamsPlayed)) {
                uksort($teamsPlayed, function($a, $b) use ($teamsPlayed) {
                    return $teamsPlayed[$b]['count'] - $teamsPlayed[$a]['count'];
                });
            }
        }

        return $teamsPlayed;
    }
    
    /*
     * Grabs the most played/rival user
     * 
     * @param User $user
     * @param array $games
     * @return array
     */
    private function getRivalUser() {
        $rival = null;
        
        if (!empty($this->user) && count($this->games) > 0) {
            $usersPlayed = array();
            foreach ($this->games as $game) {
                if ($game->confirmed == 1) {
                    $hu = $game->home_user_id;
                    $vu = $game->visitor_user_id;
                    $hun = $game->home_user_name;
                    $vun = $game->visitor_user_name;
                    $hs = $game->home_score;
                    $vs = $game->visitor_score;
                    $ot = $game->isOT;

                    # Setup the teams played array per team
                    if ($hu != $this->user->id && !array_key_exists($hu, $usersPlayed)) {
                        $usersPlayed[$hu] = array(
                            'id' => $hu,
                            'name' => $hun,
                            'count' => 0, 
                            'wins' => 0,
                            'losses' => 0,
                            'ties' => 0,
                            'otlosses' => 0,
                        );
                    }
                    if ($vu != $this->user->id && !array_key_exists($vu, $usersPlayed)) {
                        $usersPlayed[$vu] = array(
                            'id' => $vu,
                            'name' => $vun,
                            'count' => 0, 
                            'wins' => 0,
                            'losses' => 0,
                            'ties' => 0,
                            'otlosses' => 0,
                        );
                    }

                    # Add to the user played count if it's not the user
                    if ($hu != $this->user->id) {
                        $usersPlayed[$hu]['count']++;

                        # User lost to rival
                        if ($hs > $vs) {
                            $usersPlayed[$hu]['losses']++;
                            /*
                            # but it was in OT
                            if ($ot) {
                                $usersPlayed[$hu]['otlosses']++;

                            # .. LOL,JK it's in regulation HUEHUEHUE
                            } else {
                                $usersPlayed[$hu]['losses']++;
                            }
                             */

                        # User beat rival
                        } else if ($hs < $vs) {
                            $usersPlayed[$hu]['wins']++;

                        # A tie!
                        } else {
                            $usersPlayed[$hu]['ties']++;
                        }
                    # Must be away team
                    } else {
                        $usersPlayed[$vu]['count']++;

                        # User lost to rival
                        if ($hs < $vs) {
                            $usersPlayed[$vu]['losses']++;
                            /*
                            # but it was in OT
                            if ($ot) {
                                $usersPlayed[$vu]['otlosses']++;

                            # .. LOL,JK it's in regulation HUEHUEHUE
                            } else {
                                $usersPlayed[$vu]['losses']++;
                            }
                             */

                        # User beat rival
                        } else if ($hs > $vs) {
                            $usersPlayed[$vu]['wins']++;

                        # A tie!
                        } else {
                            $usersPlayed[$vu]['ties']++;
                        }
                    }
                }
            }

            if (!empty($usersPlayed)) {
                # Reverse Sort these baddies by how often they're played
                uksort($usersPlayed, function($a, $b) use ($usersPlayed) {
                    return $usersPlayed[$b]['count'] - $usersPlayed[$a]['count'];
                });

                # Grab the keys (user ids)
                $userKeys = array_keys($usersPlayed);

                # Then grab the user based on the first userKey (most played)
                $rival['stats'] = $usersPlayed[$userKeys[0]];
                
            }
        }
        
        return $rival;
    }
    
    /**
     * Average scoring!
     */
    private function getAverages() {
        $avgs = array(
            'gamesPlayed' => 0,
            'p1' => 0,
            'p2' => 0,
            'p3' => 0,
            'total' => 0,
            'shots' => 0,
            'shGoals' => 0,
            'oneTimerGoals' => 0,
            'oneTimers' => 0,
            'faceoffs' => 0,
            'bodyChecks' => 0,
            'attackZone' => 0,
            'passesReceived' => 0,
            'passes' => 0,
            'p1Against' => 0,
            'p2Against' => 0,
            'p3Against' => 0,
            'totalAgainst' => 0,
            'shotsAgainst' => 0,
            'shGoalsAgainst' => 0,
            'oneTimerGoalsAgainst' => 0,
            'oneTimersAgainst' => 0,
            'faceoffsAgainst' => 0,
            'bodyChecksAgainst' => 0,
            'attackZoneAgainst' => 0,
            'passesReceivedAgainst' => 0,
            'passesAgainst' => 0
        );
        
        $gamesPlayed = 0;
        
        $p1 = 0;
        $p2 = 0;
        $p3 = 0;
        $goals = 0;
        $shots = 0;
        $shGoals = 0;
        $oneTimerGoals = 0;
        $oneTimers = 0;
        $faceoffs = 0;
        $bodyChecks = 0;
        $attackZone = 0;
        $passesReceived = 0;
        $passes = 0;
        
        $p1Against = 0;
        $p2Against = 0;
        $p3Against = 0;
        $goalsAgainst = 0;
        $shotsAgainst = 0;
        $shGoalsAgainst = 0;
        $oneTimerGoalsAgainst = 0;
        $oneTimersAgainst = 0;
        $faceoffsAgainst = 0;
        $bodyChecksAgainst = 0;
        $attackZoneAgainst = 0;
        $passesReceivedAgainst = 0;
        $passesAgainst = 0;
        
        if (!empty($this->games)) {
            foreach ($this->games as $game) {
                if ($game->confirmed == 1) {
                    $gamesPlayed++;

                    # Hey, they're the home team!
                    if ($this->user->id == $game->home_user_id) {
                        $p1 += $game->home_p1_goals;
                        $p2 += $game->home_p2_goals;
                        $p3 += $game->home_p3_goals;
                        $goals += $game->home_score;

                        $shots += $game->home_shots;
                        $shGoals += $game->home_sh_goals;
                        $oneTimerGoals += $game->home_one_timer_goals;
                        $oneTimers += $game->home_one_timers;
                        $faceoffs += $game->home_faceoffs_won;
                        $bodyChecks += $game->home_body_checks;
                        $attackZone += $game->home_attack_zone;
                        $passesReceived += $game->home_passes_received;
                        $passes += $game->home_passes;

                        $p1Against += $game->visitor_p1_goals;
                        $p2Against += $game->visitor_p2_goals;
                        $p3Against += $game->visitor_p3_goals;
                        $goalsAgainst += $game->visitor_score;

                        $shotsAgainst += $game->visitor_shots;
                        $shGoalsAgainst += $game->visitor_sh_goals;
                        $oneTimerGoalsAgainst += $game->visitor_one_timer_goals;
                        $oneTimersAgainst += $game->visitor_one_timers;
                        $faceoffsAgainst += $game->visitor_faceoffs_won;
                        $bodyChecksAgainst += $game->visitor_body_checks;
                        $attackZoneAgainst += $game->visitor_attack_zone;
                        $passesReceivedAgainst += $game->visitor_passes_received;
                        $passesAgainst += $game->visitor_passes;
                    # They're visitor, sucka
                    } else {
                        $p1 += $game->visitor_p1_goals;
                        $p2 += $game->visitor_p2_goals;
                        $p3 += $game->visitor_p3_goals;
                        $goals += $game->visitor_score;

                        $shots += $game->visitor_shots;
                        $shGoals += $game->visitor_sh_goals;
                        $oneTimerGoals += $game->visitor_one_timer_goals;
                        $oneTimers += $game->visitor_one_timers;
                        $faceoffs += $game->visitor_faceoffs_won;
                        $bodyChecks += $game->visitor_body_checks;
                        $attackZone += $game->visitor_attack_zone;
                        $passesReceived += $game->visitor_passes_received;
                        $passes += $game->visitor_passes;

                        $p1Against += $game->home_p1_goals;
                        $p2Against += $game->home_p2_goals;
                        $p3Against += $game->home_p3_goals;
                        $goalsAgainst += $game->home_score;

                        $shotsAgainst += $game->home_shots;
                        $shGoalsAgainst += $game->home_sh_goals;
                        $oneTimerGoalsAgainst += $game->home_one_timer_goals;
                        $oneTimersAgainst += $game->home_one_timers;
                        $faceoffsAgainst += $game->home_faceoffs_won;
                        $bodyChecksAgainst += $game->home_body_checks;
                        $attackZoneAgainst += $game->home_attack_zone;
                        $passesReceivedAgainst += $game->home_passes_received;
                        $passesAgainst += $game->home_passes;
                    }
                }
            }
            $avgs['gamesPlayed'] = $gamesPlayed;
            if ($p1 > 0) $avgs['p1'] = round($p1 / $gamesPlayed, 2);
            if ($p2 > 0) $avgs['p2'] = round($p2 / $gamesPlayed, 2);
            if ($p3 > 0) $avgs['p3'] = round($p3 / $gamesPlayed, 2);
            if ($goals > 0) $avgs['goals'] = round($goals / $gamesPlayed, 2);
            if ($shots > 0) $avgs['shots'] = round($shots / $gamesPlayed, 2);
            if ($shGoals > 0) $avgs['shGoals'] = round($shGoals / $gamesPlayed, 2);
            if ($oneTimerGoals > 0) $avgs['oneTimerGoals'] = round($oneTimerGoals / $gamesPlayed, 2);
            if ($oneTimers > 0) $avgs['oneTimers'] = round($oneTimers / $gamesPlayed, 2);
            if ($faceoffs > 0) $avgs['faceoffs'] = round($faceoffs / $gamesPlayed, 2);
            if ($bodyChecks > 0) $avgs['bodyChecks'] = round($bodyChecks / $gamesPlayed, 2);
            if ($attackZone > 0) $avgs['attackZone'] = round($attackZone / $gamesPlayed, 2);
            if ($passesReceived > 0) $avgs['passesReceived'] = round($passesReceived / $gamesPlayed, 2);
            if ($passes > 0) $avgs['passes'] = round($passes / $gamesPlayed, 2);

            if ($p1Against > 0) $avgs['p1Against'] = round($p1Against / $gamesPlayed, 2);
            if ($p2Against > 0) $avgs['p2Against'] = round($p2Against / $gamesPlayed, 2);
            if ($p3Against > 0) $avgs['p3Against'] = round($p3Against / $gamesPlayed, 2);
            if ($goalsAgainst > 0) $avgs['goalsAgainst'] = round($goalsAgainst / $gamesPlayed, 2);
            if ($shotsAgainst > 0) $avgs['shotsAgainst'] = round($shotsAgainst / $gamesPlayed, 2);
            if ($shGoalsAgainst > 0) $avgs['shGoalsAgainst'] = round($shGoalsAgainst / $gamesPlayed, 2);
            if ($oneTimerGoalsAgainst > 0) $avgs['oneTimerGoalsAgainst'] = round($oneTimerGoalsAgainst / $gamesPlayed, 2);
            if ($oneTimersAgainst > 0) $avgs['oneTimersAgainst'] = round($oneTimersAgainst / $gamesPlayed, 2);
            if ($faceoffsAgainst > 0) $avgs['faceoffsAgainst'] = round($faceoffsAgainst / $gamesPlayed, 2);
            if ($bodyChecksAgainst > 0) $avgs['bodyChecksAgainst'] = round($bodyChecksAgainst / $gamesPlayed, 2);
            if ($attackZoneAgainst > 0) $avgs['attackZoneAgainst'] = round($attackZoneAgainst / $gamesPlayed, 2);
            if ($passesReceivedAgainst > 0) $avgs['passesReceivedAgainst'] = round($passesReceivedAgainst / $gamesPlayed, 2);
            if ($passesAgainst > 0) $avgs['passesAgainst'] = round($passesAgainst / $gamesPlayed, 2);
        } else {
            $this->gamesPlayed = 0;
        }
        
        return $avgs;
    }
    
    private function getTotals() {
        $totals = array(
            "wins" => 0,
            "losses" => 0,
            "ties" => 0,
            "otlosses" => 0,
            "points" => 0
        );
        
        if (!empty($this->games)) {
            foreach ($this->games as $game) {
                if ($game->confirmed == 1) {
                    $hs = $game->home_score;
                    $vs = $game->visitor_score;
                    $ot = $game->isOT;

                    /* They were the home team */
                    if ($this->user->id == $game->home_user_id) {

                        // They won
                        if ($hs > $vs) {
                            $totals['wins']++;

                        // They lost in regulation
                        } else if ($hs < $vs) {
                            $totals['losses']++;

                        // They tied
                        } else if ($hs == $vs) {
                            $totals['ties']++;
                        }

                    /* They were the visitor team */
                    } else {
                        // They won
                        if ($vs > $hs) {
                            $totals['wins']++;

                        // They lost in regulation
                        } else if ($vs < $hs) {
                            $totals['losses']++;

                        // They tied
                        } else if ($vs == $hs) {
                            $totals['ties']++;
                        }
                    }

                }
            }
        }
        
        $totals['points'] = ($totals['wins'] * 2) + $totals['ties'];
        
        return $totals;
    }
    
}