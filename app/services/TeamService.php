<?php

namespace app\services;

use \app\models\Team;

use \PDO;
use \PDOException;

class TeamService {
    
    /* make this easy on the queries */
    const TEAM_TABLE = 'teams';
    const TEAM_ID = 'id';
    const TEAM_CITY = 'city';
    const TEAM_NAME = 'name';
    const TEAM_RATING = 'rating';
    
    protected $db = null;

    public function __construct($db = null) {
        if (!isset($db)) {
            throw new Exception("Database Connection Error.");
        }
        
        $this->db = $db;
    }
   
    /**
     * Grab a team by name
     * 
     * @param String $name
     * @return Team
     */
    public function getTeamByName($name) {
        $team = null;
        if (isset($name) && !empty($name)) {
            try {
                $nameArray = explode('-', $name);
                $teamName = "%". array_pop($nameArray);
                $s = $this->db->prepare("SELECT * FROM `". self::TEAM_TABLE ."` WHERE `". self::TEAM_NAME ."` LIKE :name");
                $s->bindParam(':name', $teamName);
                $s->execute();
                $results = $s->fetch(PDO::FETCH_ASSOC);
                if (!empty($results)) {
                    $team = new Team(
                                $results[self::TEAM_ID],
                                $results[self::TEAM_CITY],
                                $results[self::TEAM_NAME],
                                $results[self::TEAM_RATING]
                            );
                }
            } catch (PDOException $e) {
                #echo $e->getMessage();
            }
        }
        return $team;
    }
    
    /**
     * Get all teams
     * @return Array
     */
    public function getAllTeams() {
        try {
            $sql = "SELECT * FROM `". self::TEAM_TABLE ."` ORDER BY `". self::TEAM_ID ."` ASC";
            $s = $this->db->query($sql);
            $results = $s->fetchAll(PDO::FETCH_ASSOC);
            
            $teamList = array();
            
            if (!empty($results)) {
                foreach ($results as $row) {
                    $teamList[] = new Team(
                                    $row[self::TEAM_ID],
                                    $row[self::TEAM_CITY],
                                    $row[self::TEAM_NAME],
                                    $row[self::TEAM_RATING]
                                );
                }
            }
        } catch (PDOException $e) {
            #echo $e->getMessage();
        }
        return $teamList;
    }
}