<?php

namespace app\routes;
use \app\controllers\GameController;
use \Flight;

$gameController = new GameController(Flight::get('twig'));

Flight::route('GET /games/', function() use (&$gameController) {
    $gameController->listGames();
});

Flight::route('GET /game/upload/', function() use (&$gameController) {
    $gameController->uploadGame();
});

Flight::route('POST /game/upload/', function() use (&$gameController) {
    $gameController->handleUploadGame();
});

Flight::route('GET /game/@id/review', function($id) use (&$gameController) {
    $gameController->reviewGame($id);
});

Flight::route('POST /game/@id/review', function($id) use (&$gameController) {
    $gameController->handleReviewGame($id);
});

# For numbered urls only (old)
Flight::route('GET /game/@id:[0-9]+/', function($id) use (&$gameController) {
    $gameController->getGameDetails($id);
});

Flight::route('GET /game/@info/', function($info) use (&$gameController) {
    $infoArray = explode('-', $info);
    $id = $infoArray[0];
    $gameController->getGameDetails($id);
});

