<?php

namespace app\routes;
use \app\controllers\SnesController;
use \Flight;

$snesController = new SnesController(Flight::get('twig'));

Flight::route('GET /snes/', function() use (&$snesController) {
    $snesController->listGames();
});

Flight::route('GET /snes/games/', function() use (&$snesController) {
    $snesController->listGames();
});

Flight::route('GET /snes/users/', function() use (&$snesController) {
    $snesController->listUsers();
});

Flight::route('GET /snes/getting-started/', function() use (&$snesController) {
    $snesController->gettingStarted();
});
