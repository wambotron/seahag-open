<?php

namespace app\routes;
use \app\controllers\ApiController;
use \Flight;

$apiController = new ApiController();

Flight::route('GET /api/users/', function() use (&$apiController) {
    $apiController->listUsers();
});

Flight::route('/api/games/@start/@limit/', function($start = 0, $limit = 10) use (&$apiController) {
    $apiController->getGames($start, $limit);
});