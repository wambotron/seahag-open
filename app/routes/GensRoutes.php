<?php

namespace app\routes;
use \app\controllers\GensController;
use \Flight;

$gensController = new GensController(Flight::get('twig'));

Flight::route('GET /gens/', function() use (&$gensController) {
    $gensController->listGames();
});

Flight::route('GET /gens/games/', function() use (&$gensController) {
    $gensController->listGames();
});

Flight::route('GET /gens/users/', function() use (&$gensController) {
    $gensController->listUsers();
});

Flight::route('GET /gens/getting-started/', function() use (&$gensController) {
    $gensController->gettingStarted();
});
