<?php

namespace app\routes;
use \app\controllers\TeamController;
use \Flight;

$teamController = new TeamController(Flight::get('twig'));

Flight::route('GET /teams/', function() use (&$teamController) {
    $teamController->listTeams();
});

Flight::route('GET /team/@name/', function($name) use (&$teamController) {
    $teamController->getTeamDetails($name);
});