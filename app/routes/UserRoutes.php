<?php

namespace app\routes;
use \app\controllers\UserController;
use \Flight;

$userController = new UserController(Flight::get('twig'));

Flight::route('GET /signup/', function() use (&$userController) {
    $userController->signup();
});

Flight::route('POST /signup/', function() use (&$userController) {
    $userController->handleSignup();
});

Flight::route('GET /login/', function() use (&$userController) {
    $userController->login();
});

Flight::route('POST /login/', function() use (&$userController) {
    $userController->handleLogin();
});

Flight::route('/logout/', function() use (&$userController) {
    $userController->logout();
});

Flight::route('/users/', function() use (&$userController) {
    $userController->listUsers();
});

Flight::route('GET /users/ratings/update/', function() use (&$userController) {
    $userController->updateRatings();
    Flight::redirect('/users/');
});

Flight::route('GET /user/reset-password/', function() use (&$userController) {
    $userController->resetPassword();
});

Flight::route('POST /user/reset-password/', function() use (&$userController) {
    $userController->handleResetPassword();
});

Flight::route('GET /user/edit/', function() use (&$userController) {
    $userController->editUser();
});

Flight::route('POST /user/edit/', function() use (&$userController) {
    $userController->handleEditUser();
});

Flight::route('/user/@name/unconfirmed/', function($name) use (&$userController) {
    $userController->getUserDetailsUnconfirmed($name);
});

Flight::route('/user/@name/as/@team/', function($name, $team) use (&$userController) {
    $userController->getUserDetailsAsTeam($name, $team);
});

Flight::route('/user/@name/', function($name) use (&$userController) {
    $userController->getUserDetails($name);
});
