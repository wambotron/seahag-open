<?php

/* Vendor Libraries */
require_once ROOT.'/flight/Flight.php';
require_once ROOT.'/Twig/Autoloader.php';
require_once ROOT.'/sendgrid/SendGrid_loader.php';

/* Begin the session */
session_start();

/* Initilialize Twig */
Twig_Autoloader::register();
$loader = new Twig_Loader_Filesystem('app/views');

/* db init */
$twig = new Twig_Environment($loader, array(
    'cache' => ROOT.'/app/views/cache'
));

require_once('inc/db-config.php');

// Set your sendgrid info here (if using it)
Flight::set('sendgrid_username', '');
Flight::set('sendgrid_password', '');

$rostersSnes = json_decode(file_get_contents(ROOT.'/static/js/rosters.json'));
$rostersGens = json_decode(file_get_contents(ROOT.'/static/js/rosters-gens.json'));

$twig->addGlobal('cookie', $_COOKIE);
$twig->addGlobal('session', $_SESSION);
$twig->addGlobal('rostersSnes', $rostersSnes);
$twig->addGlobal('rostersGens', $rostersGens);

Flight::set('twig', $twig);

try {
    $db = new PDO("mysql:host=$server;port=$port;dbname=$database", $user, $password);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    #echo $e->getMessage();
}

Flight::set('db', $db);