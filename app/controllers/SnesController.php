<?php

namespace app\controllers;

use \app\services\GameService;
use \app\services\JsonDataService;

use \Flight;

class SnesController {
    
    protected $twig = null;
    
    public function __construct($twig = null) {
        if (!isset($twig)) {
            throw new Exception("Template Error.");
        }
        
        $this->twig = $twig;
    }
    
    /**
     * List all SNES games
     */
    public function listGames() {
        $jsonService = new JsonDataService(Flight::get('db'));
        $games = $jsonService->getConfirmedGamesBySystemId(1, 0, 10);
        
        if ($games && count($games) > 0) {
            $games = json_decode($games)->games;
        }
        
        echo $this->twig->render('snes/game-list.twig', array('games' => $games));
    }
    
    /**
     * List all SNES players
     */
    public function listUsers() {
        $jsonService = new JsonDataService(Flight::get('db'));
        $users = json_decode($jsonService->getAllUsersRecentRecordsBySystemId(1))->users;
        
        echo $this->twig->render('snes/user-list.twig', array('users' => $users));
    }
    
    public function gettingStarted() {
        echo $this->twig->render('snes/getting-started.twig');
    }
    
}