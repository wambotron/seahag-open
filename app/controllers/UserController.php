<?php

namespace app\controllers;

use \app\services\UserService;
use \app\services\UserStatService;
use \app\services\JsonDataService;
use \app\services\TeamService;
use \app\services\GameService;
use \app\models\Glicko2Player;
use \app\models\User;
use \app\models\Cookie;
use \Flight;
use \SendGrid;

class UserController {
    
    protected $twig = null;
    
    public function __construct($twig = null) {
        if (!isset($twig)) {
            throw new Exception("Template Error.");
        }
        
        $this->twig = $twig;
    }
    
    /**
     * GET handler, the login page itself
     */
    public function login() {
        echo $this->twig->render('login.twig');
    }
    
    /**
     * POST handler, handles the login process, redirects to user page
     */
    public function handleLogin() {
        /* default message */
        $message = 'Something suddenly came up!';
        
        /* Make sure everything came in */
        if (isset($_POST['name']) && isset($_POST['password'])) {
            $userService = new UserService(Flight::get('db'));
            $user = $userService->checkUserCredentials($_POST['name'], $_POST['password']);
            
            /* They successfully logged in */
            if (!empty($user)) {
                /* set their session variables */
                Cookie::setUser($user);

                /* and redirect them to their own page */
                Flight::redirect('/user/'. strtolower($_POST['name']));
            } else {
                /* Invalid login */
                $message = 'The username/password combination is incorrect. Please try again.';
            }
        } else {
            /* They didn't enter all the form fields */
            $message = 'Please enter username and password to login.';
        }
        
        echo $this->twig->render('login.twig', array('message' => $message));
    }
    
    /**
     * Edit user email or password
     */
    public function editUser() {
        /* Try to grab the current user, otherwise redirect */
        if (isset($_COOKIE['userName']) && $_COOKIE['userName'] != '') {
            $userService = new UserService(Flight::get('db'));
            $user = $userService->getUserById($_COOKIE['userId']);
            
            echo $this->twig->render('user/edit.twig', array('user' => $user));
        } else {
            Flight::redirect('/');
        }
        
    }
    
    /**
     * Handle the edit request
     */
    public function handleEditUser() {
        if (isset($_COOKIE['userName']) && $_COOKIE['userName'] != '') {
            /* default message */
            $message = false;
            $success = false;
            $userService = new UserService(Flight::get('db'));
            $currentUser = $userService->getUserById($_COOKIE['userId']);

            /* Make sure everything came in */
            if (isset($_POST['email']) && !empty($_POST['email']) && isset($_POST['oldPass'])) {
                $user = $userService->checkUserCredentials($_COOKIE['userName'], $_POST['oldPass']);

                /* Yeah, that's the right cred */
                if (!empty($user)) {
                    if (isset($_POST['pass1']) && !empty($_POST['pass1']) && isset($_POST['pass2'])) {
                        if (!empty($_POST['pass1']) && $_POST['pass1'] === $_POST['pass2']) {
                            if (!empty($_POST['contact'])) {
                                $user->contact = $_POST['contact'];
                            }
                            $user->password = $_POST['pass1'];
                            $user->email = $_POST['email'];

                            if ($userService->editUser($user)) {
                                $success = "You're good to go.";
                                $currentUser->password = $user->password;
                                $currentUser->email = $user->email;
                                $currentUser->contact = $user->contact;
                            }
                        } else {
                            $message = "You have mismatching passwords.";
                        }
                    } else if ($_POST['email'] != $user->email) {
                        if (!empty($_POST['contact'])) {
                            $user->contact = $_POST['contact'];
                        }
                        $user->password = $_POST['oldPass'];
                        $user->email = $_POST['email'];
                        
                        if ($userService->editUser($user)) {
                            $success = "Bam, email changed.";
                            $currentUser->email = $user->email;
                            $currentUser->contact = $user->contact;
                        } else {
                            $message = "Something went wrong. Try again!";
                        }
                    } else if ($_POST['contact'] != $user->contact) {
                            $user->contact = $_POST['contact'];
                            $user->password = $_POST['oldPass'];
                            
                            if ($userService->editUser($user)) {
                                $success = "AIM name changed.";
                                $currentUser->email = $user->email;
                                $currentUser->contact = $user->contact;
                            } else {
                                $message = "Something went wrong. Try again!";
                            }
                    } else {
                        $message = "Nothing to see here!";
                    }
                } else {
                    /* Invalid login */
                    $message = 'The username/password combination is incorrect. Please try again.';
                }
            } else {
                /* They didn't enter all the form fields */
                $message = 'Please enter username and password to login.';
            }
            echo $this->twig->render('user/edit.twig', array('user' => $currentUser, 'message' => $message, 'success' => $success));
        } else {
            Flight::redirect('/');
        }
    }
    
    public function resetPassword() {
        echo $this->twig->render('user/reset-password.twig');
    }
    
    public function handleResetPassword() {
        /* default message */
        $message = false;
        $success = false;
        
        /* They didn't enter all the form fields */
        if (empty($_POST['name']) || empty($_POST['email'])){
            $message = 'Please enter username and email.'; 
            
        /* We have a username and email, time to compare... */
        } else {
            
            $username = trim($_POST['name']);
            $email = trim($_POST['email']);
            
            /* attempt to load user */
            $userService = new UserService(Flight::get('db'));
            $user = $userService->getUserByName($username);

            /* User exists */
            if (!empty($user) && $user->email == $email) {

                /* Attempt to generate and assign new password */
                $newPass = $userService->returnRandomPassword();
                if(($user->password = $newPass) && $userService->editUser($user)){
                    $success = "Your password has been reset. Please check your email.";
                    $sendGrid = new SendGrid(Flight::get('sendgrid_username'), Flight::get('sendgrid_password'));
                    $mail = new SendGrid\Mail();
                    $mail->addTo($user->email)
                            ->setFrom('no-reply@domain.com')
                            ->setSubject('Your password has been reset')
                            ->setHtml('<p>Your new password is: <b>'. $newPass .'</b></p><p>You should probably <a href="">login</a> and change it to something more memorable.</p>');
                    $sendGrid->smtp->send($mail);
                }
                else{
                    $message = 'Interesting. Something broke! Try again?';
                }
                
            /* Invalid user */
            } else {
                $message = "Eh? Username and email don't match anyone I know...";
            }
        }
        
        echo $this->twig->render('user/reset-password.twig', 
            array('message' => $message, 
                'success' => $success,
                'name' => $_POST['name'],
                'email' => $_POST['email'],
                )
            );
    }

    /**
     * This should just redirect to the homepage after it destroys the session
     */
    public function logout() {
        session_destroy();
        Cookie::destroy();
        Flight::redirect('/');
    }
	  
    
    /**
     * GET handler
     */
    public function signup() {
        echo $this->twig->render('sign-up.twig');
    }
    
    /**
     * POST handler for signup submissions
     */
    public function handleSignup() {
        $message = 'Something suddenly came up!';
        /* Make sure everything came in */
        if (isset($_POST['name']) 
            && !empty($_POST['name'])
            && isset($_POST['email'])
            && !empty($_POST['email'])
            && isset($_POST['pass1'])
            && !empty($_POST['pass1'])) {
            /* Their username is a word.. */
            if (preg_match("/^\w+$/", $_POST['name'])) {
                /* and the passwords match */
                if ($_POST['pass1'] == $_POST['pass2']) {
                    /* create a new user object with the provided details */
                    $user = new User(null, $_POST['name'], $_POST['email'], $_POST['contact'], $_POST['pass1']);

                    $userService = new UserService(Flight::get('db'));
                    /* Make sure the user is created */
                    $newUserId = $userService->createNewUser($user);
                    if ($newUserId > 0) {
                        /* set their session variables */
                        $user->id = $newUserId;
                        Cookie::setUser($user);

                        /* and redirect them to their own page */
                        Flight::redirect('/user/'. strtolower($_POST['name']));
                    } else {
                        /* User by that name must already exist */
                        $message = 'That username is taken.';
                    }
                } else {
                    /* Passwords don't match */
                    $message = 'Passwords do not match.';
                }
            } else {
                $message = "Names have to be alphanumeric";
            }
        } else {
            /* All fields were not entered */
            $message = 'You must complete all fields to sign up.';
        }
        /* Something went wrong along the way.. send them back to the signup page */
        
        echo $this->twig->render('sign-up.twig', array('message' => $message));
    }
    
    /**
     * Displays user details based on user name.
     * 
     * @param String $name
     */
    public function getUserDetails($name) {
        $jsonService = new JsonDataService(Flight::get('db'));
        $userJson = json_decode($jsonService->getFullUserDetailsByUserName($name));
        
        $data['user'] = $userJson->users[0];
        
        if (!empty($userJson)) {
            $gamesJson = json_decode($jsonService->getGamesByUserId($userJson->users[0]->id, 0, 5000));
            
            if (!empty($gamesJson)) {
                $userStatService = new UserStatService($userJson->users[0], $gamesJson->games);
                
                #$data['unconfirmed'] = $this->getUnconfirmedGames($games);
                $data['games'] = $gamesJson->games;
                $data['userStats'] = $userStatService->getAllUserStatDetails();
            }
            
        }
        echo $this->twig->render('user/details.twig', $data);
    }

    /**
     * Displays user details of unconfirmed games based on user name.
     * 
     * @param String $name
     */
    public function getUserDetailsUnconfirmed($name) {
        $jsonService = new JsonDataService(Flight::get('db'));
        $userJson = json_decode($jsonService->getBasicUserDetailsByUserName($name));

        $games = array("games" => array());
        
        if (!empty($userJson) && count($userJson->users) > 0) {
            $jsonService = new JsonDataService(Flight::get('db'));
            $games = $jsonService->getUnconfirmedAndDeniedGamesByUserId($userJson->users[0]->id, 0,100);
        }
        
        echo $this->twig->render('user/unconfirmed.twig', array('games' => json_decode($games)->games));
    }

    /**
     *  Displays user details by confirmed games with a given team
     *
     * @param String $name
     * @param String $team
     */
    public function getUserDetailsAsTeam($name, $team) {
        $jsonService = new JsonDataService(Flight::get('db'));
        $userJson = json_decode($jsonService->getFullUserDetailsByUserName($name));

        $teamService = new TeamService(Flight::get('db'));
        $team = $teamService->getTeamByName($team);
        
        $data['user'] = $userJson->users[0];
        
        if (!empty($userJson) && !empty($team)) {
            $gamesJson = json_decode($jsonService->getGamesAsTeamByUserId($team->id, $userJson->users[0]->id, 0, 5000));
            
            if (!empty($gamesJson)) {
                $userStatService = new UserStatService($userJson->users[0], $gamesJson->games);
                
                #$data['unconfirmed'] = $this->getUnconfirmedGames($games);
                $data['games'] = $gamesJson->games;
                $data['userStats'] = $userStatService->getAllUserStatDetails();
            }
            
        }
        echo $this->twig->render('user/as-team.twig', $data);
    }


    
    /**
     * Displays all users
     */
    public function listUsers() {
        $jsonService = new JsonDataService(Flight::get('db'));
        $users = json_decode($jsonService->getAllUsersRecentRecords())->users;
        
        echo $this->twig->render('user/list.twig', array('users' => $users));
    }

    public function updateRatings() {
        $s = new JsonDataService(Flight::get('db'));
        $userService = new UserService(Flight::get('db'));

        $results = json_decode($s->getConfirmedGamesBasic(0, 10000));
        $userlist = json_decode($s->getAllUserMinimalDetails());

        $games = array_reverse($results->games);
        $users = array();

        foreach ($userlist->users as $user) {
            $users[$user->id] = array(
                'glicko' => new Glicko2Player(),
                'id' => $user->id,
                'name' => $user->name
            );
        }

        foreach ($games as $game) {
            $home = $users[$game->home_user_id]['glicko'];
            $visitor = $users[$game->visitor_user_id]['glicko'];

            if ($game->home_score > $game->visitor_score) {
                $home->AddWin($visitor);
                $visitor->AddLoss($home);
            } else if ($game->visitor_score > $game->home_score) {
                $visitor->AddWin($home);
                $home->AddLoss($visitor);
            } else {
                $home->AddDraw($visitor);
                $visitor->AddDraw($home);
            }

            foreach ($users as $user) {
                $user['glicko']->Update();
            }
        }

        foreach ($users as $user) {
            $rating = floor($user['glicko']->rating);
            $id = $user['id'];
            $userService->updateUserRatingByUserId($id, $rating);
        }
    }
    
    /**
     * Sets up any notifications for a user
     */
    public function setupNotifications() {
        if (isset($_COOKIE['userId']) && !empty($_COOKIE['userId'])) {
            $JsonDataService = new JsonDataService(Flight::get('db'));  
            $unconfirmedGames = $JsonDataService->getUnconfirmedAndDeniedGamesByUserId($_COOKIE['userId'], 0, 100);

            $games = json_decode($unconfirmedGames)->games;
            $this->twig->addGlobal('unconfirmedGames', count($games));
        }
    }
    
    /** 
     * Grabs unconfirmed games for notification purposes
     * 
     * @param array $games
     */
    private function getUnconfirmedGames($games) {
        $unconfirmed = array();
        
        if (!empty($games) && count($games) > 0) {
            foreach ($games as $game) {
                if ($game['confirmed'] == 0) {
                    $unconfirmed[] = $game;
                }
            }
        }
        
        return $unconfirmed;
    }
    
}