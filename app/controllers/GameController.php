<?php

namespace app\controllers;

use \app\services\GameService;
use \app\services\UserService;
use \app\services\TeamService;
use \app\services\StatService;
use \app\services\JsonDataService;

use \app\extractors\NHL94GameStatExtractor;
use \app\extractors\NHL94GenesisStatExtractor;

use \app\models\Game;
use \app\models\NHL94GameStat;

use \Flight;

class GameController {
    
    protected $twig = null;
    
    public function __construct($twig = null) {
        if (!isset($twig)) {
            throw new Exception("Template Error.");
        }
        
        $this->twig = $twig;
    }
    
    /**
     * Get game details
     * 
     * @param Integer $id
     */
    public function getGameDetails($id) {
        $jsonService = new JsonDataService(Flight::get('db'));
        $game = $jsonService->getAllGameDetailsByGameId($id);
        
        $details = array();
        
        if ($game) {
            $deJsonGames = json_decode($game)->games[0];
            $deJsonGames->penalty_summary = json_decode($deJsonGames->penalty_summary);
            $deJsonGames->scoring_summary = json_decode($deJsonGames->scoring_summary);
            $details['game'] = $deJsonGames;
        }
        
        echo $this->twig->render('game/details.twig', $details);
    }
    
    /**
     * Displays recent games
     */
    public function listGames() {
        $jsonService = new JsonDataService(Flight::get('db'));
        $games = $jsonService->getConfirmedGamesBasic(0, 10);
        
        echo $this->twig->render('game/list.twig', array('games' => json_decode($games)->games));
    }
    
    /**
     * Show the upload form
     */
    public function uploadGame() {
         if (isset($_COOKIE['userName']) && !empty($_COOKIE['userName'])) {
            $jsonService = new JsonDataService(Flight::get('db'));
            $users = $jsonService->getAllUserMinimalDetails();
            echo $this->twig->render('game/upload.twig', array('users' => json_decode($users)->users));
        } else {
            Flight::redirect('/');
        }
    }
    
    /**
     * Upload the file, make the game, create the stats!
     */
    public function handleUploadGame() {
         if (isset($_COOKIE['userName']) && !empty($_COOKIE['userName'])) {
            /* default message */
            $message = 'Something suddenly came up!';
            
            /* Make sure they sent everything in */
            if (isset($_POST['home_visitor']) 
                && isset($_POST['opponent']) 
                && $_POST['opponent'] != "Choose Someone!"
                && isset($_FILES['state']) 
                && $_FILES['state']['size'] > 0) {
                
                /* zsnes saves are 268kb, but we'll give some buffer */
                if ($_FILES['state']['size'] < 280000) {
                    /* Let's make sure it has a valid extension while we're at it */
                    $match = preg_match_all('/^.*\.(zst|zs[0-9]|gs[0-9])$/i', strtolower($_FILES['state']['name']), $matches);
                    if ($_FILES['state']['tmp_name'] > '') {
                        if ($match) {
                            if (preg_match('/^.*gs[0-9]$/i', $matches[1][0])) {
                                $system = 2; # Genesis
                            } else {
                                $system = 1; # SNES
                            }
                            
                            /* It seems to be a valid file, let's move it over */
                            $targetPath = ROOT. '/ups/'. $_FILES['state']['name'];
                            
                            # move the file to a temporary directory
                            if (move_uploaded_file($_FILES['state']['tmp_name'], $targetPath)) {
                                /* That went off without a hitch, now let's create a new game and grab some stats */
                                
                                if ($system === 1) {
                                    $statExtractor = new NHL94GameStatExtractor($targetPath);
                                } else {
                                    $statExtractor = new NHL94GenesisStatExtractor($targetPath);
                                }
                                $stats = $statExtractor->getStats();
                                
                                if (!empty($stats)) {
                                    /* Let's figure out who was home and visitor */
                                    if ($_POST['home_visitor'] == 'home') {
                                        $homeUser = $_COOKIE['userId'];
                                        $visitorUser = $_POST['opponent'];
                                    } else {
                                        $homeUser = $_POST['opponent'];
                                        $visitorUser = $_COOKIE['userId'];
                                    }
                                    
                                    /* We also need to know if it went into overtime */
                                    if ($stats['home_ot_goals'] > 0 # they scored in OT
                                    || $stats['visitor_ot_goals'] > 0 # or they did
                                    || ($stats['home_goals'] == $stats['visitor_goals'] # or their scores are the same
                                        && $stats['home_ot_goals'] == $stats['visitor_ot_goals'])) { # and no one scored in OT
                                        $isOT = 1;
                                    } else {
                                        $isOT = 0;
                                    }
                                    
                                    /* Great, we got the stats from it! .. now we need to make a game */
                                    $gameService = new GameService(Flight::get('db'));
                                    $newGame = new Game(
                                        null, # id - newly created, so we don't have one
                                        $homeUser,
                                        $visitorUser,
                                        $stats['home_goals'],
                                        $stats['visitor_goals'],
                                        $stats['home_team'],
                                        $stats['visitor_team'],
                                        $isOT,
                                        5,
                                        $_COOKIE['userId'], # uploader
                                        $system # SNES or Genesis
                                    );

                                    # This will return the game ID
                                    $gameId = $gameService->createNewGame($newGame);
                                    
                                    if ($gameId > 0) {
                                        /* create a new GameStat! */
                                        $gameStat = new NHL94GameStat(
                                            false, # no id yet
                                            $gameId, 
                                            $homeUser,
                                            $visitorUser,
                                            $stats['home_goals'],
                                            $stats['visitor_goals'],
                                            $stats['home_team'],
                                            $stats['visitor_team'],
                                            $stats['home_p1_goals'],
                                            $stats['home_p2_goals'],
                                            $stats['home_p3_goals'],
                                            $stats['home_ot_goals'],
                                            $stats['visitor_p1_goals'],
                                            $stats['visitor_p2_goals'],
                                            $stats['visitor_p3_goals'],
                                            $stats['visitor_ot_goals'],
                                            $stats['home_pp_goals'],
                                            $stats['visitor_pp_goals'],
                                            $stats['home_pp'],
                                            $stats['visitor_pp'],
                                            $stats['home_attack_zone'],
                                            $stats['visitor_attack_zone'],
                                            $stats['home_pp_time'],
                                            $stats['visitor_pp_time'],
                                            $stats['home_pp_shots'],
                                            $stats['visitor_pp_shots'],
                                            $stats['home_sh_goals'],
                                            $stats['visitor_sh_goals'],
                                            $stats['home_faceoffs_won'],
                                            $stats['visitor_faceoffs_won'],
                                            $stats['home_body_checks'],
                                            $stats['visitor_body_checks'],
                                            $stats['home_passes_received'],
                                            $stats['visitor_passes_received'],
                                            $stats['home_passes'],
                                            $stats['visitor_passes'],
                                            $stats['home_breakaways'],
                                            $stats['visitor_breakaways'],
                                            $stats['home_breakaway_goals'],
                                            $stats['visitor_breakaway_goals'],
                                            $stats['home_penalty_shots'],
                                            $stats['visitor_penalty_shots'],
                                            $stats['home_penalty_shot_goals'],
                                            $stats['visitor_penalty_shot_goals'],
                                            $stats['home_one_timers'],
                                            $stats['visitor_one_timers'],
                                            $stats['home_one_timer_goals'],
                                            $stats['visitor_one_timer_goals'],
                                            $stats['home_shots'],
                                            $stats['visitor_shots'],
                                            $stats['penalty_summary'],
                                            $stats['scoring_summary']
                                        );
                                        
                                        // get rid of the file
                                        unlink($targetPath);
                                        $statService = new StatService(Flight::get('db'));
                                        if ($statService->saveNHL94Game($gameStat)) {
                                            /* Stats are in, too. Redirect them to the game */
                                            Flight::redirect('/game/'. $gameId .'/review/');
                                        } else {
                                            $message = "Something went wrong saving the stats..";
                                        }
                                    } else {
                                        $message = "The game just doesn't want to exist. Try it again!";
                                    }
                                    
                                } else {
                                    $message = "The stat extraction didn't go as planned. Please try again.";
                                }
                            } else {
                                $message = "Error uploading the file. Please try again.";
                            }
                        } else {
                            $message = 'Wrong file extension bud.';
                        }
                    } else {
                        $message = "Error uploading the file. Please try again.";
                    }
                } else {
                    $message = 'Hmm, you might want to double check the file you uploaded.';
                }
            } else {
                $message = 'You .. need to enter more stuff than that.';
            }
            
            
            $jsonService = new JsonDataService(Flight::get('db'));
            $users = $jsonService->getAllUserMinimalDetails();
            echo $this->twig->render('game/upload.twig', array('users' => json_decode($users)->users, 'message' => $message));
        } else {
            Flight::redirect('/');
        }
    }
    
    public function reviewGame($id) {
        if (isset($_COOKIE['userName']) && !empty($_COOKIE['userName'])) {
            # details array for the game/errors
            $details = array();
            
            if (is_numeric($id)) {
                # they sent in an id and such, but let's make sure they are part of the game
                $gameService = new GameService(Flight::get('db'));
                $game = $gameService->getGameById($id);
                if (!empty($game) && $this->canEdit($game)) {
                    # Yeah, they can edit it
                    $details = $this->getReviewDetails($id);
                    echo $this->twig->render('game/review.twig', $details);
                } else {
                    Flight::redirect('/game/'. $id .'/');
                }
            } else {
                Flight::redirect('/game/'. $id .'/');
            }
        } else {
            Flight::redirect('/');
        }
    }
    
    
    
    public function handleReviewGame() {
        if (isset($_COOKIE['userName']) && !empty($_COOKIE['userName'])) {
            $message = 'Something suddenly came up!';
            
            # Good game ID?
            if (isset($_POST['id'])
            && is_numeric($_POST['id'])) {
                
                # They're verifying post-upload
                if (isset($_POST['correct'])
                && !empty($_POST['correct'])) {
                
                    # they sent in an id and such, but let's make sure they are part of the game
                    $gameService = new GameService(Flight::get('db'));
                    $game = $gameService->getGameById($_POST['id']);
                    if (!empty($game) && $this->canEdit($game)) {
                        # Yeah, they can edit it
                        # If it's all correct, we can just redirect them to the game
                        if ($_POST['correct'] == "yes" && $gameService->resetGameConfirmation($_POST['id'])) {
                            Flight::redirect('/game/'.$_POST['id']);

                        # oh god, something was messed up. Delete the stats
                        } else if ($_POST['correct'] == "no") {
                            $gameService = new GameService(Flight::get('db'));
                            $statService = new StatService(Flight::get('db'));

                            if ($gameService->deleteGameById($_POST['id']) && $statService->deleteNHLGameStatById($_POST['id'])) {
                                # OK, we killed it. Redirect them to the upload form
                                Flight::redirect('/game/upload/');
                            } else {
                                $message = "Oh .. well, this is embarrassing. We can't fix that right now. Try again!";
                            }
                        }
                    }

                # They're confirming someone else's upload
                } else if (isset($_POST['confirm'])
                && !empty($_POST['confirm'])) {
                    
                    # The game is all set, confirm it and move on
                    if ($_POST['confirm'] == "yes") {
                        $gameService = new GameService(Flight::get('db'));
                        
                        if ($gameService->confirmGame($_POST['id'])) {
                            Flight::redirect('/game/'. $_POST['id'] .'/');
                        } else {
                            #It didn't save correctly, maybe the game ID was wrong?
                            $message = "Hmm, something is wonky here. Try again. If this persists, contact an admin!";
                        }

                    # Something is weird. They won't confirm it. Set it to 2 and redirect them
                    } else {
                        $gameService = new GameService(Flight::get('db'));
                        
                        if ($gameService->denyGame($_POST['id'])) {
                            Flight::redirect('/user/'. $_COOKIE['userName'] .'/');
                        } else {
                            #It didn't save correctly, maybe the game ID was wrong?
                            $message = "Hmm, something is wonky here. Try again. If this persists, contact an admin!";
                        }
                    }
                    
                } else {
                    $message = "You didn't actually say whether it was good or not.";
                }
                
                #
            } else {
                $message = "Something about that game isn't quite right...";
            }
            
            $details = $this->getReviewDetails($_POST['id']);
            $details['message'] = $message;
            
            echo $this->twig->render('game/review.twig', $details);
        } else {
            Flight::redirect('/');
        }
    }
    
    /**
     * Check to see if a user is a member of a game/admin
     * 
     * @param Game $game
     * @return Boolean
     */
    private function canEdit($game) {
        return (!empty($game) 
                && ($game->confirmed == 0 # reviewed, not confirmed 
                    && ($game->homeUser == $_COOKIE['userId'] 
                        || $game->visitorUser == $_COOKIE['userId']))
                || ($game->confirmed == 2 # denied - the uploader can delete it or resubmit
                    && ($game->homeUser == $_COOKIE['userId'] 
                        || $game->visitorUser == $_COOKIE['userId'])
                    && $game->uploader == $_COOKIE['userId'])
                || ($game->confirmed == 5 # not reviewed
                    && ($game->homeUser == $_COOKIE['userId'] 
                        || $game->visitorUser == $_COOKIE['userId'])
                    && $game->uploader == $_COOKIE['userId']));
    }
    
    /**
     * Return the details of a game for review
     * 
     * @param Integer $id
     * @return array
     */
    private function getReviewDetails($id) {
        $jsonService = new JsonDataService(Flight::get('db'));
        $game = $jsonService->getAllGameDetailsByGameId($id);

        $details = array();

        if ($game) {
            $deJsonGames = json_decode($game)->games[0];
            $deJsonGames->penalty_summary = json_decode($deJsonGames->penalty_summary);
            $deJsonGames->scoring_summary = json_decode($deJsonGames->scoring_summary);
            
            $details['game'] = $deJsonGames;
        }
        
        return $details;
    }
    
}
