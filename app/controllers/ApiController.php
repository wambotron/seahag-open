<?php

namespace app\controllers;

use \app\services\JsonDataService;

use \Flight;

class ApiController {
    
    public function __construct($twig = null) {}
    
    /**
     * Displays recent games
     */
    public function listUsers() {
        $jsonService = new JsonDataService(Flight::get('db'));
        echo $jsonService->getAllUserMinimalDetails();
    }

    public function getGames($start = 0, $limit = 10) {
        $jsonService = new JsonDataService(Flight::get('db'));
        echo $jsonService->getConfirmedGamesBasic($start, $limit);
    }
}
