<?php

namespace app\controllers;

use \app\services\TeamService;
use \app\services\GameService;
use \app\services\TeamStatService;
use \app\services\JsonDataService;
use \Flight;

class TeamController {
    
    protected $twig = null;
    
    public function __construct($twig = null) {
        if (!isset($twig)) {
            throw new Exception("Template Error.");
        }
        
        $this->twig = $twig;
    }
    
    /**
     * Get team details
     * 
     * @param Integer $id
     */
    public function getTeamDetails($name) {
        $teamService = new TeamService(Flight::get('db'));
        $team = $teamService->getTeamByName($name);
        
        $jsonService = new JsonDataService(Flight::get('db'));
        $games = $jsonService->getConfirmedGamesByTeamId($team->id, 0, 1000);

        if ($games && count($games) > 0) {
            $games = json_decode($games)->games;
        }
        
        $teamStats = null;
        if (!empty($games)) {
            $teamStatService = new TeamStatService($team, $games);
            $teamStats = $teamStatService->getTeamStatDetails();
        }

        echo $this->twig->render('team/details.twig', array('team' => $team, 'teamStats' => $teamStats));
    }
    
    /**
     * Displays all teams
     */
    public function listTeams() {
        $teamService = new TeamService(Flight::get('db'));
        $teams = $teamService->getAllTeams();
        
        echo $this->twig->render('team/list.twig', array('teams' => $teams));
    }
    
}