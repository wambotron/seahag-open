<?php

namespace app\controllers;

use \app\services\GameService;
use \app\services\JsonDataService;

use \Flight;

class GensController {
    
    protected $twig = null;
    
    public function __construct($twig = null) {
        if (!isset($twig)) {
            throw new Exception("Template Error.");
        }
        
        $this->twig = $twig;
    }
    
    /**
     * List all SNES games
     */
    public function listGames() {
        $jsonService = new JsonDataService(Flight::get('db'));
        $games = $jsonService->getConfirmedGamesBySystemId(2, 0, 10);
        
        if ($games && count($games) > 0) {
            $games = json_decode($games)->games;
        }
        
        echo $this->twig->render('gens/game-list.twig', array('games' => $games));
    }
    
    /**
     * List all SNES players
     */
    public function listUsers() {
        $jsonService = new JsonDataService(Flight::get('db'));
        $users = json_decode($jsonService->getAllUsersRecentRecordsBySystemId(2))->users;
        
        echo $this->twig->render('gens/user-list.twig', array('users' => $users));
    }
    
    public function gettingStarted() {
        echo $this->twig->render('gens/getting-started.twig');
    }
    
}