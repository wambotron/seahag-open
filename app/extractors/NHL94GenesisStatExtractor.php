<?php

namespace app\extractors;

class NHL94GenesisStatExtractor {
    
    /* Hex locations for stats */
    
    # AA BB CC DD EE FF GG
    # AA - Time in seconds
    # BB - Period
    # CC - Team Status- HOME- 00 SH 2, 01 SH, 02 EV, 03 PP, 04 PP 2; AWAY- 10 SH 2, 11 SH, 12 EV, 13 PP, 14 PP 2
    # DD - Scoring player 00-18
    # EE - Assist 1 (FF if none)
    # FF - Assist 2 (FF if none)
    const SCORING_SUMMARY = 59627;
    
    # AA - time in seconds
    # BB - period where 00, 40, 80, C0 are periods
    # CC - Player, home starts at 00, away at 80
    # DD - penalty -- see penalty map for descriptions
    const PENALTY_SUMMARY = 59989; 
    
    
    const HOME_TEAM = 59305;
    const HOME_SHOTS = 60231; # total
    const HOME_POWER_PLAY_GOALS = 60233;
    const HOME_POWER_PLAY = 60235;
    const HOME_ATTACK_ZONE = 60240; # and 60241 -- first is seconds, second is a 4:16s modifier (256 seconds)
    const HOME_GOALS = 60243;
    const HOME_FACEOFFS_WON = 60245;
    const HOME_BODY_CHECKS = 60247;
    const HOME_PASSES = 60249;
    const HOME_PASSES_RECEIVED = 60251;
    const HOME_P1_GOALS = 61065;
    const HOME_P2_GOALS = 61067;
    const HOME_P3_GOALS = 61069;
    const HOME_OT_GOALS = 61071;
    const HOME_P1_SHOTS = 61073;
    const HOME_P2_SHOTS = 61075;
    const HOME_P3_SHOTS = 61077;
    const HOME_OT_SHOTS = 61079;
    const HOME_POWER_PLAY_TIME = 61080;
    const HOME_POWER_PLAY_SHOTS = 61083;
    const HOME_SHORTHANDED_GOALS = 61085;
    const HOME_BREAKAWAYS = 61087;
    const HOME_BREAKAWAY_GOALS = 61089;
    const HOME_PENALTY_SHOTS = 61095;
    const HOME_PENALTY_SHOT_GOALS = 61097;
    const HOME_ONE_TIMERS = 61091;
    const HOME_ONE_TIMER_GOALS = 61093;
    
    
    const VISITOR_TEAM = 59307;
    const VISITOR_SHOTS = 61099;
    const VISITOR_POWER_PLAY_GOALS = 61101;
    const VISITOR_POWER_PLAY = 61103;
    const VISITOR_ATTACK_ZONE = 61108;
    const VISITOR_GOALS = 61111;
    const VISITOR_FACEOFFS_WON = 61113;
    const VISITOR_BODY_CHECKS = 61115;
    const VISITOR_PASSES = 61117;
    const VISITOR_PASSES_RECEIVED = 61119;
    const VISITOR_P1_GOALS = 61933;
    const VISITOR_P2_GOALS = 61935;
    const VISITOR_P3_GOALS = 61937;
    const VISITOR_OT_GOALS = 61939;
    const VISITOR_P1_SHOTS = 61941;
    const VISITOR_P2_SHOTS = 61943;
    const VISITOR_P3_SHOTS = 61945;
    const VISITOR_OT_SHOTS = 61947;
    const VISITOR_POWER_PLAY_TIME = 61948;
    const VISITOR_POWER_PLAY_SHOTS = 61951;
    const VISITOR_SHORTHANDED_GOALS = 61953;
    const VISITOR_BREAKAWAYS = 61955;
    const VISITOR_BREAKAWAY_GOALS = 61957;
    const VISITOR_ONE_TIMERS = 61959;
    const VISITOR_ONE_TIMER_GOALS = 61961;
    const VISITOR_PENALTY_SHOTS = 61963;
    const VISITOR_PENALTY_SHOT_GOALS = 61965;
    
    protected $penaltyMap = array(
        18 => 'Roughing',
        22 => 'Charging',
        24 => 'Slashing',
        26 => 'Roughing',
        28 => 'Cross Check',
        30 => 'Hooking',
        32 => 'Tripping',
        34 => 'Penalty Shot',
        36 => 'Interference',
        38 => 'Holding'
    );
    
    protected $periodMap = array(
        0 => '1st',
        1 => '1st',
        64 => '2nd',
        65 => '2nd',
        128 => '3rd',
        129 => '3rd',
        192 => 'OT',
        193 => 'OT',
        12 => 'OT'
    );
    
    protected $stats = array();

    public function __construct($filename) {
        if (empty($filename)) {
            throw new Exception("ERROR: No file associated.");
        }
        
        $state = fopen($filename, 'rb');
        
        /* Grab these in order of appearance */
        /* Our DB is 1-index, the game is 0-index. Add one to get the right team! */
        fseek($state, self::HOME_TEAM);
        $stats['home_team'] = $this->getPart($state) + 1;
        
        # Grab the scoring summary!
        $stats['scoring_summary'] = json_encode($this->getScoringSummary($state));
        
        # And the penalty summary
        $stats['penalty_summary'] = json_encode($this->getPenaltySummary($state));
        
        fseek($state, self::HOME_SHOTS);
        $stats['home_shots'] = $this->getPart($state);
        
        fseek($state, self::HOME_POWER_PLAY_GOALS);
        $stats['home_pp_goals'] = $this->getPart($state);
        
        fseek($state, self::HOME_POWER_PLAY);
        $stats['home_pp'] = $this->getPart($state);
        
        fseek($state, self::HOME_ATTACK_ZONE);
        $temp = $this->getPart($state); # first part -- modifier, seconds * 256
        $temp2 = $this->getPart($state); # second part -- actual seconds
        $stats['home_attack_zone'] = $temp2 + ($temp * 256);
        
        fseek($state, self::HOME_GOALS);
        $stats['home_goals'] = $this->getPart($state);
        
        fseek($state, self::HOME_FACEOFFS_WON);
        $stats['home_faceoffs_won'] = $this->getPart($state);
        
        fseek($state, self::HOME_BODY_CHECKS);
        $stats['home_body_checks'] = $this->getPart($state);
        
        fseek($state, self::HOME_PASSES);
        $stats['home_passes'] = $this->getPart($state);
        
        fseek($state, self::HOME_PASSES_RECEIVED);
        $stats['home_passes_received'] = $this->getPart($state);
        
        fseek($state, self::HOME_P1_GOALS);
        $stats['home_p1_goals'] = $this->getPart($state);
        
        fseek($state, self::HOME_P2_GOALS);
        $stats['home_p2_goals'] = $this->getPart($state);
        
        fseek($state, self::HOME_P3_GOALS);
        $stats['home_p3_goals'] = $this->getPart($state);
        
        fseek($state, self::HOME_OT_GOALS);
        $stats['home_ot_goals'] = $this->getPart($state);
        
        fseek($state, self::HOME_POWER_PLAY_TIME);
        $temp = $this->getPart($state); # first part -- modifier, seconds * 256
        $temp2 = $this->getPart($state); # second part -- actual seconds
        $stats['home_pp_time'] = $temp2 + ($temp * 256);
        
        fseek($state, self::HOME_POWER_PLAY_SHOTS);
        $stats['home_pp_shots'] = $this->getPart($state);
        
        fseek($state, self::HOME_SHORTHANDED_GOALS);
        $stats['home_sh_goals'] = $this->getPart($state);
        
        fseek($state, self::HOME_BREAKAWAYS);
        $stats['home_breakaways'] = $this->getPart($state);
        
        fseek($state, self::HOME_BREAKAWAY_GOALS);
        $stats['home_breakaway_goals'] = $this->getPart($state);
        
        fseek($state, self::HOME_ONE_TIMERS);
        $stats['home_one_timers'] = $this->getPart($state);
        
        fseek($state, self::HOME_ONE_TIMER_GOALS);
        $stats['home_one_timer_goals'] = $this->getPart($state);
        
        fseek($state, self::HOME_PENALTY_SHOTS);
        $stats['home_penalty_shots'] = $this->getPart($state);
        
        fseek($state, self::HOME_PENALTY_SHOT_GOALS);
        $stats['home_penalty_shot_goals'] = $this->getPart($state);
        
        
        
        
        fseek($state, self::VISITOR_TEAM);
        $stats['visitor_team'] = $this->getPart($state) + 1;
        
        fseek($state, self::VISITOR_SHOTS);
        $stats['visitor_shots'] = $this->getPart($state);
        
        fseek($state, self::VISITOR_POWER_PLAY_GOALS);
        $stats['visitor_pp_goals'] = $this->getPart($state);
        
        fseek($state, self::VISITOR_POWER_PLAY);
        $stats['visitor_pp'] = $this->getPart($state);
        
        fseek($state, self::VISITOR_ATTACK_ZONE);
        $temp = $this->getPart($state); # first part
        $temp2 = $this->getPart($state); # second part!
        $stats['visitor_attack_zone'] = $temp2 + ($temp * 256);
        
        fseek($state, self::VISITOR_GOALS);
        $stats['visitor_goals'] = $this->getPart($state);
        
        fseek($state, self::VISITOR_FACEOFFS_WON);
        $stats['visitor_faceoffs_won'] = $this->getPart($state);
        
        fseek($state, self::VISITOR_BODY_CHECKS);
        $stats['visitor_body_checks'] = $this->getPart($state);
        
        fseek($state, self::VISITOR_PASSES);
        $stats['visitor_passes'] = $this->getPart($state);
        
        fseek($state, self::VISITOR_PASSES_RECEIVED);
        $stats['visitor_passes_received'] = $this->getPart($state);
        
        fseek($state, self::VISITOR_P1_GOALS);
        $stats['visitor_p1_goals'] = $this->getPart($state);
        
        fseek($state, self::VISITOR_P2_GOALS);
        $stats['visitor_p2_goals'] = $this->getPart($state);
        
        fseek($state, self::VISITOR_P3_GOALS);
        $stats['visitor_p3_goals'] = $this->getPart($state);
        
        fseek($state, self::VISITOR_OT_GOALS);
        $stats['visitor_ot_goals'] = $this->getPart($state);
        
        fseek($state, self::VISITOR_POWER_PLAY_TIME);
        $temp = $this->getPart($state); # first part
        $temp2 = $this->getPart($state); # second part!
        $stats['visitor_pp_time'] = $temp2 + ($temp * 256);
        
        fseek($state, self::VISITOR_POWER_PLAY_SHOTS);
        $stats['visitor_pp_shots'] = $this->getPart($state);
        
        fseek($state, self::VISITOR_SHORTHANDED_GOALS);
        $stats['visitor_sh_goals'] = $this->getPart($state);
        
        fseek($state, self::VISITOR_BREAKAWAYS);
        $stats['visitor_breakaways'] = $this->getPart($state);
        
        
        fseek($state, self::VISITOR_BREAKAWAY_GOALS);
        $stats['visitor_breakaway_goals'] = $this->getPart($state);
        
        fseek($state, self::VISITOR_ONE_TIMERS);
        $stats['visitor_one_timers'] = $this->getPart($state);
        
        fseek($state, self::VISITOR_ONE_TIMER_GOALS);
        $stats['visitor_one_timer_goals'] = $this->getPart($state);
        
        fseek($state, self::VISITOR_PENALTY_SHOTS);
        $stats['visitor_penalty_shots'] = $this->getPart($state);
        
        fseek($state, self::VISITOR_PENALTY_SHOT_GOALS);
        $stats['visitor_penalty_shot_goals'] = $this->getPart($state);
       
        /* That's all we need for now */
        fclose($state);
        $this->stats = $stats;
    }
    
    # AA - time in seconds
    # BB - period where 00, 40, 80, C0 are periods
    # CC - penalty -- see penalty map for descriptions
    # DD - Player, home starts at 00, away at 80
    private function getPenaltySummary($state) {
        $summaries = array();
        
        fseek($state, self::PENALTY_SUMMARY);
        $penalties = hexdec(bin2hex(fread($state, 1)));
        
        if ($penalties > 0) {
            $penalties /= 4; # 4 bytes per penalty info
        
            # 65 possible penalties
            for ($i = 0; $i < $penalties; $i++) {
                $summaryTP = $this->getSummaryTimeAndPeriod($state);
                $time = $summaryTP['time'];
                $period = $summaryTP['period'];

                $penalty = $this->getPart($state);
                $player = $this->getPart($state);
                $team = 'home';

                if ($penalty > 150) {
                    $team = 'visitor';
                    $penalty -= 128; 
                }
                
                $summaries[] = array(
                    "time" => $time,
                    "period" => $this->periodMap[$period],
                    "player" => $player,
                    "penalty" => $this->penaltyMap[$penalty],
                    "team" => $team
                );
            }
        }
        
        return $summaries;
    }
    
    # AA BB CC DD EE FF GG
    # AA - Period
    # BB - Time in seconds
    # CC - Team Status- HOME- 00 SH 2, 01 SH, 02 EV, 03 PP, 04 PP 2; AWAY- 80 SH 2, 81 SH, 82 EV, 83 PP, 84 PP 2
    # DD - Scoring player 00-18
    # EE - Assist 1 (FF if none)
    # FF - Assist 2 (FF if none)
    private function getScoringSummary($state) {
        $summaries = array();
        
        fseek($state, self::SCORING_SUMMARY);
        $goals = hexdec(bin2hex(fread($state, 1)));
        
        if ($goals > 0) {
            $goals /= 6; # 6 bytes per goal info

            for ($i = 0; $i < $goals; $i++) {
                $summaryTP = $this->getSummaryTimeAndPeriod($state);
                $time = $summaryTP['time'];
                $period = $summaryTP['period'];
                $status = $this->getPart($state);
                $scorer = $this->getPart($state);
                $assistant1 = $this->getPart($state);
                $assistant2 = $this->getPart($state);
                $team = 'home';

                if ($status >= 16) {
                    $team = 'visitor';
                    $status -= 128; # to set it back to 0
                }

                $summaries[] = array(
                    "time" => $time,
                    "period" => $this->periodMap[$period],
                    "status" => $status,
                    "scorer" => $scorer,
                    "assistant1" => $assistant1,
                    "assistant2" => $assistant2,
                    "team" => $team
                );
            }
        }
        
        return $summaries;
    }
    
    /*
     * Grab the Time and Period
     * 
     * @return array(time, period)
     */
    private function getSummaryTimeAndPeriod($state) {
        $period = bin2hex(fread($state, 1));
        $time = bin2hex(fread($state, 1));
        
        return array(
            'time' => hexdec(substr($period, -1) . $time),
            'period' => hexdec(substr($period, 0))
        );
    }
    
    private function getPart($state) {
        return hexdec(bin2hex(fread($state, 1)));
    }
    
    /**
     * Grab the stats after they're parsed.
     * 
     * @return array
     */
    public function getStats() {
        return $this->stats;
    } 
}