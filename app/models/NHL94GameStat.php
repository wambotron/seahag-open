<?php

namespace app\models;

class NHL94GameStat {
    
    public $id = '';
    public $game = '';
    public $homeUser = '';
    public $visitorUser = '';
    public $homeScore = 0;
    public $visitorScore = 0;
    public $homeTeam = 0;
    public $visitorTeam = 0;
    public $homeGoalsP1 = 0;
    public $homeGoalsP2 = 0;
    public $homeGoalsP3 = 0;
    public $homeGoalsOT = 0;
    public $visitorGoalsP1 = 0;
    public $visitorGoalsP2 = 0;
    public $visitorGoalsP3 = 0;
    public $visitorGoalsOT = 0;
    public $homePPGoals = 0;
    public $visitorPPGoals = 0;
    public $homePP = 0;
    public $visitorPP = 0;
    public $homeAttackZone = 0;
    public $visitorAttackZone = 0;
    public $homePPTime = 0;
    public $visitorPPTime = 0;
    public $homePPShots = 0;
    public $visitorPPShots = 0;
    public $homeSHGoals = 0;
    public $visitorSHGoals = 0;
    public $homeFaceoffsWon = 0;
    public $visitorFaceoffsWon = 0;
    public $homeBodyChecks = 0;
    public $visitorBodyChecks = 0;
    public $homePassesReceived = 0;
    public $visitorPassesReceived = 0;
    public $homePasses = 0;
    public $visitorPasses = 0;
    public $homeBreakaways = 0;
    public $visitorBreakaways = 0;
    public $homeBreakawayGoals = 0;
    public $visitorBreakawayGoals = 0;
    public $homePenaltyShots = 0;
    public $visitorPenaltyShots = 0;
    public $homePenaltyShotGoals = 0;
    public $visitorPenaltyShotGoals = 0;
    public $homeOneTimers = 0;
    public $visitorOneTimers = 0;
    public $homeOneTimerGoals = 0;
    public $visitorOneTimerGoals = 0;
    public $homeShots = 0;
    public $visitorShots = 0;
    public $penaltySummary = "";
    public $scoringSummary = "";
    
    public function __construct($id = null, 
                                $game = false,
                                $homeUser = false,
                                $visitorUser = false,
                                $homeScore = false,
                                $visitorScore = false,
                                $homeTeam = false,
                                $visitorTeam = false,
                                $homeGoalsP1 = false,
                                $homeGoalsP2 = false,
                                $homeGoalsP3 = false,
                                $homeGoalsOT = false,
                                $visitorGoalsP1 = false,
                                $visitorGoalsP2 = false,
                                $visitorGoalsP3 = false,
                                $visitorGoalsOT = false,
                                $homePPGoals = false,
                                $visitorPPGoals = false,
                                $homePP = false,
                                $visitorPP = false,
                                $homeAttackZone = false,
                                $visitorAttackZone = false,
                                $homePPTime = false,
                                $visitorPPTime = false,
                                $homePPShots = false,
                                $visitorPPShots = false,
                                $homeSHGoals = false,
                                $visitorSHGoals = false,
                                $homeFaceoffsWon = false,
                                $visitorFaceoffsWon = false,
                                $homeBodyChecks = false,
                                $visitorBodyChecks = false,
                                $homePassesReceived = false,
                                $visitorPassesReceived = false,
                                $homePasses = false,
                                $visitorPasses = false,
                                $homeBreakaways = false,
                                $visitorBreakaways = false,
                                $homeBreakawayGoals = false,
                                $visitorBreakawayGoals = false,
                                $homePenaltyShots = false,
                                $visitorPenaltyShots = false,
                                $homePenaltyShotGoals = false,
                                $visitorPenaltyShotGoals = false,
                                $homeOneTimers = false,
                                $visitorOneTimers = false,
                                $homeOneTimerGoals = false,
                                $visitorOneTimerGoals = false,
                                $homeShots = false,
                                $visitorShots = false,
                                $penaltySummary = false,
                                $scoringSummary = false) {
        
        if (is_numeric($game)
        && is_numeric($homeUser)
        && is_numeric($visitorUser)
        && is_numeric($homeTeam)
        && is_numeric($visitorTeam)) {
            if (!empty($id) && is_numeric($id)) $this->id = $id;
            
            $this->game = $game;
            
            $this->homeUser = $homeUser;
            $this->visitorUser = $visitorUser;
            
            if (!empty($homeScore) && is_numeric($homeScore)) $this->homeScore = $homeScore;
            if (!empty($visitorScore) && is_numeric($visitorScore)) $this->visitorScore = $visitorScore;
            
            $this->homeTeam = $homeTeam;
            $this->visitorTeam = $visitorTeam;
            
            if (!empty($homeGoalsP1) && is_numeric($homeGoalsP1)) $this->homeGoalsP1 = $homeGoalsP1;
            if (!empty($visitorGoalsP1) && is_numeric($visitorGoalsP1)) $this->visitorGoalsP1 = $visitorGoalsP1;
            if (!empty($homeGoalsP2) && is_numeric($homeGoalsP2)) $this->homeGoalsP2 = $homeGoalsP2;
            if (!empty($visitorGoalsP2) && is_numeric($visitorGoalsP2)) $this->visitorGoalsP2 = $visitorGoalsP2;
            if (!empty($homeGoalsP3) && is_numeric($homeGoalsP3)) $this->homeGoalsP3 = $homeGoalsP3;
            if (!empty($visitorGoalsP3) && is_numeric($visitorGoalsP3)) $this->visitorGoalsP3 = $visitorGoalsP3;
            if (!empty($homeGoalsOT) && is_numeric($homeGoalsOT)) $this->homeGoalsOT = $homeGoalsOT;
            if (!empty($visitorGoalsOT) && is_numeric($visitorGoalsOT)) $this->visitorGoalsOT = $visitorGoalsOT;
            
            if (!empty($homePPGoals) && is_numeric($homePPGoals)) $this->homePPGoals = $homePPGoals;
            if (!empty($visitorPPGoals) && is_numeric($visitorPPGoals)) $this->visitorPPGoals = $visitorPPGoals;
            
            if (!empty($homePP) && is_numeric($homePP)) $this->homePP = $homePP;
            if (!empty($visitorPP) && is_numeric($visitorPP)) $this->visitorPP = $visitorPP;
            
            if (!empty($homeAttackZone) && is_numeric($homeAttackZone)) $this->homeAttackZone = $homeAttackZone;
            if (!empty($visitorAttackZone) && is_numeric($visitorAttackZone)) $this->visitorAttackZone = $visitorAttackZone;
            
            if (!empty($homePPTime) && is_numeric($homePPTime)) $this->homePPTime = $homePPTime;
            if (!empty($visitorPPTime) && is_numeric($visitorPPTime)) $this->visitorPPTime = $visitorPPTime;
            
            if (!empty($homePPShots) && is_numeric($homePPShots)) $this->homePPShots = $homePPShots;
            if (!empty($visitorPPShots) && is_numeric($visitorPPShots)) $this->visitorPPShots = $visitorPPShots;
            
            if (!empty($homeSHGoals) && is_numeric($homeSHGoals)) $this->homeSHGoals = $homeSHGoals;
            if (!empty($visitorSHGoals) && is_numeric($visitorSHGoals)) $this->visitorSHGoals = $visitorSHGoals;
            
            if (!empty($homeFaceoffsWon) && is_numeric($homeFaceoffsWon)) $this->homeFaceoffsWon = $homeFaceoffsWon;
            if (!empty($visitorFaceoffsWon) && is_numeric($visitorFaceoffsWon)) $this->visitorFaceoffsWon = $visitorFaceoffsWon;
            
            if (!empty($homeBodyChecks) && is_numeric($homeBodyChecks)) $this->homeBodyChecks = $homeBodyChecks;
            if (!empty($visitorBodyChecks) && is_numeric($visitorBodyChecks)) $this->visitorBodyChecks = $visitorBodyChecks;
            
            if (!empty($homePassesReceived) && is_numeric($homePassesReceived)) $this->homePassesReceived = $homePassesReceived;
            if (!empty($visitorPassesReceived) && is_numeric($visitorPassesReceived)) $this->visitorPassesReceived = $visitorPassesReceived;
            
            if (!empty($homePasses) && is_numeric($homePasses)) $this->homePasses = $homePasses;
            if (!empty($visitorPasses) && is_numeric($visitorPasses)) $this->visitorPasses = $visitorPasses;
            
            if (!empty($homeBreakaways) && is_numeric($homeBreakaways)) $this->homeBreakaways = $homeBreakaways;
            if (!empty($visitorBreakaways) && is_numeric($visitorBreakaways)) $this->visitorBreakaways = $visitorBreakaways;
            
            if (!empty($homeBreakawayGoals) && is_numeric($homeBreakawayGoals)) $this->homeBreakawayGoals = $homeBreakawayGoals;
            if (!empty($visitorBreakawayGoals) && is_numeric($visitorBreakawayGoals)) $this->visitorBreakawayGoals = $visitorBreakawayGoals;
            
            if (!empty($homePenaltyShots) && is_numeric($homePenaltyShots)) $this->homePenaltyShots = $homePenaltyShots;
            if (!empty($visitorPenaltyShots) && is_numeric($visitorPenaltyShots)) $this->visitorPenaltyShots = $visitorPenaltyShots;
            
            if (!empty($homePenaltyShotGoals) && is_numeric($homePenaltyShotGoals)) $this->homePenaltyShotGoals = $homePenaltyShotGoals;
            if (!empty($visitorPenaltyShotGoals) && is_numeric($visitorPenaltyShotGoals)) $this->visitorPenaltyShotGoals = $visitorPenaltyShotGoals;
            
            if (!empty($homeOneTimers) && is_numeric($homeOneTimers)) $this->homeOneTimers = $homeOneTimers;
            if (!empty($visitorOneTimers) && is_numeric($visitorOneTimers)) $this->visitorOneTimers = $visitorOneTimers;
            
            if (!empty($homeOneTimerGoals) && is_numeric($homeOneTimerGoals)) $this->homeOneTimerGoals = $homeOneTimerGoals;
            if (!empty($visitorOneTimerGoals) && is_numeric($visitorOneTimerGoals)) $this->visitorOneTimerGoals = $visitorOneTimerGoals;
            
            if (!empty($homeShots) && is_numeric($homeShots)) $this->homeShots = $homeShots;
            if (!empty($visitorShots) && is_numeric($visitorShots)) $this->visitorShots = $visitorShots;
            
            if (!empty($penaltySummary)) $this->penaltySummary = $penaltySummary;
            if (!empty($scoringSummary)) $this->scoringSummary = $scoringSummary;
        } else {
            error_log("ERROR:: NHL94SGameStat.php -- Non-numeric values.");
        }
    }
}