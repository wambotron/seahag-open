<?php

namespace app\models;

class User {
    
    public $id = '';
    public $name = '';
    public $email = '';
    public $contact = '';
    public $password = '';
    public $role = '';
    public $record = array();
    public $rating = 1500;
    
    function __construct($id = false, $name = false, $email = false, $contact = false, $password = false, $role = 0, $rating = 1500) {
        if ($name && $email && $password) {
            if (!empty($id)) $this->id = $id;
            $this->name = $name;
            $this->email = $email;
            if (!empty($contact)) $this->contact = $contact;
            $this->password = $password;
            $this->role = $role;
            $this->rating = $rating;
        } else {
            throw new \Exception ("Missing required fields");
        }
    }
}