<?php

namespace app\models;

class UserStat {

    public $favorite = ''; # most-played team
    public $teamsPlayed = '';
    public $rival = ''; # The most-played opponent
    public $nemesis = ''; # The worst winning percentage against an opponent
    public $averages = '';
    public $record = '';
    
}