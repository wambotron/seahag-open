<?php

namespace app\models;

class System {
    
    public $id = ''; # 1 is SNES, 2 is Genesis
    public $name = '';
    
    function __construct($id = false, $name = false) {
        if (!empty($id) && is_numeric($id)) $this->id = $id;
        if (!empty($name)) $this->name = $name;
    }
}