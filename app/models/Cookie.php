<?php

namespace app\models;

class Cookie {
    
    public static function setUser($user) {
        if (isset($user)) {
            $expiry = time() + 60 * 60 * 24 * 365 * 3; # 3 years!
            setcookie('userId', $user->id, $expiry, '/');
            setcookie('userName', $user->name, $expiry, '/');
        }
    }
    
    public static function destroy() {
        $expiry = time() - 3600; # an hour ago
        setcookie('userId', '', $expiry, '/');
        setcookie('userName', '', $expiry, '/');
    }
}