<?php

namespace app\models;

class Team {
    
    public $id = '';
    public $city = '';
    public $name = '';
    public $rating = '';
    
    function __construct($id = false, $city = false, $name = false, $rating = false) {
        if ($city && $name && $rating) {
            if (!empty($id) && is_numeric($id)) $this->id = $id;
            $this->city = $city;
            $this->name = $name;
            $this->rating = $rating;
        } else {
            throw new Exception ("Missing required fields");
        }
    }
}