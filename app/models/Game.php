<?php

namespace app\models;

class Game {
    
    public $id = '';
    public $homeUser = '';
    public $visitorUser = '';
    public $homeScore = 0;
    public $visitorScore = 0;
    public $homeTeam = 0;
    public $visitorTeam = 0;
    public $isOT = 0;
    public $confirmed = 0; # 0 is unconfirmed, 1 is confirmed, 2 is denied, 5 is unverified by uploader
    public $uploader = 0;
    public $system = 0; # 1 is SNES, 2 is Genesis
    
    function __construct($id = false, $homeUser = false, $visitorUser = false, $homeScore = false, $visitorScore = false, $homeTeam = false, $visitorTeam = false, $isOT = false, $confirmed = false, $uploader = false, $system = false) {
        if (is_numeric($homeUser)
            && is_numeric($visitorUser)
            && is_numeric($homeTeam)
            && is_numeric($visitorTeam)
        ) {
            
            if (!empty($id) && is_numeric($id)) $this->id = $id;
            $this->homeUser = $homeUser;
            $this->visitorUser = $visitorUser;
            if (!empty($homeScore) && is_numeric($homeScore)) $this->homeScore = $homeScore;
            if (!empty($visitorScore) && is_numeric($visitorScore)) $this->visitorScore = $visitorScore;
            $this->homeTeam = $homeTeam;
            $this->visitorTeam = $visitorTeam;
            if (!empty($isOT) && is_numeric($isOT)) $this->isOT = $isOT;
            if (!empty($confirmed) && is_numeric($confirmed)) $this->confirmed = $confirmed;
            if (!empty($uploader) && is_numeric($uploader)) $this->uploader = $uploader;
            if (!empty($system) && is_numeric($system)) $this->system = $system;
        } else {
            throw new \Exception ("Missing required fields");
        }
    }
}