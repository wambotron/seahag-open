(function ($) {
    
    var ext = "zst,zs1,zs2,zs3,zs4,zs5,zs6,zs7,zs8,zs9,gs0,gs1,gs2,gs3,gs4,gs5,gs6,gs7,gs8,gs9";
    
    var validator = new FormValidator('uploader', [{
        name: 'state',
        display: 'save state',
        rules: 'required|is_file_type['+ ext +']'
    }, {
        name: 'opponent',
        display: 'Opponent',
        rules: 'numeric'
    }, {
        name: 'home_visitor',
        display: 'Home or Visitor',
        rules: 'required'
    }], function(errors, ev) {
        if (errors.length > 0) {
            var $msg = $(".error"),
                messages = "",
                i = 0,
                l = errors.length;
                
            if ($msg.length === 0) {
                $msg = $("<div class='error'></div>").insertBefore(".form-o-matic");
            }
        
            for (; i < l; i++) {
                messages += errors[i].message + "<br>";
            }
        
            $msg.html(messages);
            return false;
        }
    });
    
    validator
        .setMessage('required', '%s is required.')
        .setMessage('is_file_type', 'You can only upload a valid ZSnes or Gens save file.')
        .setMessage('numeric', 'You have to choose an opponent.');
    
}(jQuery));