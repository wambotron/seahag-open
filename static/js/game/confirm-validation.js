(function ($) {
    
    var validator = new FormValidator('game-verification', [{
        name: 'correct',
        rules: 'required'
    }], function(errors, ev) {
        if (errors.length > 0) {
            var $msg = $(".error"),
                messages = "",
                i = 0,
                l = errors.length;
                
            if ($msg.length === 0) {
                $msg = $("<div class='error'></div>").insertBefore(".confirmation-box");
            }
        
            for (; i < l; i++) {
                messages += errors[i].message + "<br>";
            }
        
            $msg.html(messages);
            return false;
        }
    });
    validator.setMessage('required', 'You really need to verify this.');
    
    var confirmValidator = new FormValidator('game-confirmation', [{
        name: 'confirm',
        rules: 'required'
    }], function(errors, ev) {
        if (errors.length > 0) {
            var $msg = $(".error"),
                messages = "",
                i = 0,
                l = errors.length;
                
            if ($msg.length === 0) {
                $msg = $("<div class='error'></div>").insertBefore(".confirmation-box");
            }
        
            for (; i < l; i++) {
                messages += errors[i].message + "<br>";
            }
        
            $msg.html(messages);
            return false;
        }
    });
    confirmValidator.setMessage('required', 'You really need to confirm this.');
    
}(jQuery));