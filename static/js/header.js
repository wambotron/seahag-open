(function ($) {
    var isShown = false,
        handleUpload = function(event) {
            event.preventDefault();

            // Only show this on pages that are not "upload game"
            if (!isShown && location.pathname.search('game/upload') === -1) {
                try { window.scrollTo(0,1);
                } catch (ex) {};

                $.ajax('/api/users/', {
                    dataType: 'json',
                    success: function (data, status, xhr) {
                        var placeholder = $("#upload-bits"),
                            template = $("#upload-template").html();

                        placeholder.html(Mustache.render(template, data));

                        $("#upload-game").animate({
                            'margin-top': '40px'
                        }, 200, function() { isShown = true; });
                    }
                });
            }

            return false;
        };

    $('#main-navigation').on('click', '.upload', handleUpload);
}(jQuery));