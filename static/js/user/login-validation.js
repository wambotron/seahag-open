(function ($) {
    
    var validator = new FormValidator('login', [{
        name: 'name',
        display: 'Username',
        rules: 'required'
    }, {
        name: 'password',
        display: 'Password',
        rules: 'required'
    }], function(errors, ev) {
        if (errors.length > 0) {
            var $msg = $(".error"),
                messages = "",
                i = 0,
                l = errors.length;
                
            if ($msg.length === 0) {
                $msg = $("<div class='error'></div>").insertBefore(".form-o-matic");
            }
        
            for (; i < l; i++) {
                messages += errors[i].message + "<br>";
            }
        
            $msg.html(messages);
            return false;
        }
    });
    validator.setMessage('required', '%s is required.');
    
}(jQuery));