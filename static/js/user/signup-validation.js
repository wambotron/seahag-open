(function ($) {
    
    var validator = new FormValidator('signup', [{
        name: 'name',
        display: 'Username',
        rules: 'required|min_length[4]'
    }, {
        name: 'email',
        display: 'Email',
        rules: 'required'
    }, {
        name: 'pass1',
        display: 'Password',
        rules: 'required'
    }, {
        name: 'pass2',
        display: 'Re-entered password',
        rules: 'required|matches[pass1]'
    }], function(errors, ev) {
        if (errors.length > 0) {
            var $msg = $(".error"),
                messages = "",
                i = 0,
                l = errors.length;
                
            if ($msg.length === 0) {
                $msg = $("<div class='error'></div>").insertBefore(".form-o-matic");
            }
        
            for (; i < l; i++) {
                messages += errors[i].message + "<br>";
            }
        
            $msg.html(messages);
            return false;
        }
    });
    
    validator
        .setMessage('required', '%s is required.')
        .setMessage('matches', 'Passwords must match!');
    
}(jQuery));